import os
import time
from datetime import datetime, date
import pytest
from selenium.common.exceptions import NoSuchElementException

import utilities
from functions.ClientListFeatures import ClientMethods
from functions.CreateAccountFeature import CreateAccountMethods
from functions.EngagementFeatures import EngagementMethods
from functions.FirmFeatures import FirmMethods
from functions.LiveChatSupportFeature import ChatSupportMethods
from functions.LoginFeatures import LoginMethods
from functions.ReportPageFeatures import ReportMethods
from pageObjects.ClientsTab import ClientTab
from pageObjects.Clients_ClientsList import ClientsList
from pageObjects.CookiePage import Cookies
from pageObjects.EngagementsTab import Engagement
from pageObjects.MyFirmTab import FirmTab
from utilities import Data, gmailAPI
from utilities.Data import set_test_data
from utilities.fileUploadDownload import UploadsDownloads
from selenium import webdriver


# Test requires data element from stored excel testDataLocation:
#                   login email = getData["FirmUserEmail"]
#                   user login password = getData["FirmUserPassword"])
#                   client user email = getData["ClientUserEmail"]
@pytest.mark.Pipeline
@pytest.mark.RLM
@pytest.mark.SmokeTest
@pytest.mark.hookwrapper("pytest_runtest_makereport")
@pytest.mark.usefixtures("setup_method", scope="class")
class Test:
    clientName = "Smoke Test Client: "
    manualEngagementName = "[MANUAL] Engagement: "
    importEngagementName = "[IMPORT] Engagement: "
    cloneEngagementName = "[CLONE] Engagement: "
    deleteEngagementName = "[DELETE] Engagement: "
    reportName = "[REPORT] Report Name: "
    categoryName = "Category Name: "
    requestName1 = "Request Name1"
    requestName2 = "Request Name2"

    def test_smoke_test_firm(self, get_data):

        # Live Chat Support
        live_chat_support = ChatSupportMethods(self.driver)
        live_chat_support.open_chat_support_window()

        # Firm user login
        login_method = LoginMethods(self.driver)
        login_method.valid_login(get_data["FirmUserEmail"], get_data["FirmUserPassword"])

        # Create new Client
        new_clients_method = ClientMethods(self.driver)
        new_clients_method.create_client_pipeline(
            self.clientName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")),
            "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")))

        # Create new manual engagement
        set_manual_engagement_name = self.manualEngagementName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S"))
        new_clients_method.create_new_manual_engagement(set_manual_engagement_name,
                                                        "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")))

        engagements_method = EngagementMethods(self.driver)
        engagements_method.create_category_for_engagement(
            self.categoryName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")))
        engagements_method.create_request_for_engagement(self.requestName1)
        engagements_method.assign_user_to_request(get_data["FirmUserEmail"])
        engagements_method.lock_the_request()
        engagements_method.add_new_comment_to_request(
            "Add comment to the request on {}".format(datetime.now().strftime("%m%d%Y-%H%M%S")))
        uploads = UploadsDownloads(self.driver)
        engagements_method.upload_file_to_request(uploads.op_ivy_pdf())
        engagements_method.create_request_for_engagement(self.requestName2)
        engagements_method.upload_file_to_request(uploads.op_ivy_pdf())

        # Create new engagement via import
        new_clients_method.create_new_import_engagement(
            self.importEngagementName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")),
            uploads.sample_tax_entity_xlsx())

        # Create new engagement via clone
        new_clients_method.create_new_clone_engagement(
            self.cloneEngagementName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")),
            set_manual_engagement_name)

        # Publish a new report
        new_clients_method.create_new_report(self.reportName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")),
                                             uploads.report_file_pdf())

        # create Delete Me FirmTab to use for Automation Rule
        current_date = date.today()
        past_date = current_date.replace(year=2000)
        new_clients_method.create_new_manual_engagement(
            self.deleteEngagementName + "{}".format(datetime.now().strftime("%m%d%Y-%H%M%S")),
            past_date.strftime("%m/%d/%Y"))

        # Create new automation rule
        firm_method = FirmMethods(self.driver)
        firm_method.create_new_automation_rule("DELETE ENGAGEMENT RULE", "Delete", "Used to delete rules",
                                               "Engagement Name(s)", "Contains", "DELETE", "30 Days",
                                               "After Due Date", "YESDELETE")

        # Create EULA for client
        firm = FirmTab(self.driver)
        firm.company_tab().click()
        firm_method.enable_firm_eula(" You must agree to the terms found in the End License Agreement")

        # Create new client admin user
        set_client_email = get_data["ClientEmail"] + "{}".format(
            datetime.now().strftime("%m%d%Y-%H%M%S")) + "@suralink.com"
        new_clients_method.invite_client_admin_user("ClientFirst", "ClientLast", set_client_email)

        # get client user invitation link
        invitation_link = new_clients_method.get_SecureInvitaionLink_txt()

        # write data to excel
        set_test_data("I2", set_manual_engagement_name)
        set_test_data("J2", set_client_email)
        set_test_data("M2", invitation_link)

        time.sleep(7)

    def test_smoke_test_client(self, get_data):

        # Create new account
        create_account = CreateAccountMethods(self.driver)
        create_account.create_account(get_data["SecureInvitationLink"], get_data["ClientPassword"])
        time.sleep(5)

        # Search for email validation email
        gmail = utilities.gmailAPI
        results = gmail.searchMessages(gmail.service, '"Verify your account Click the link below to verify your email"')

        for msg in results:
            auth_token = gmail.getMessagePayload_VerifyEmail_parsed(gmail.service, msg)
            print("URL: " + auth_token)

            self.driver.get(auth_token)

        # Accept EULA for client
        firm_method = FirmMethods(self.driver)
        firm_method.accept_eula("Client User", "First Last", "Link-A-Sure")

        # remove Cookie notice
        cookie = Cookies(self.driver)
        try:
            cookie.got_it_btn().click()
        except NoSuchElementException:
            print("Cookie not found")

        # Find previously created engagement
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.select_engagement_lnk(get_data["EngagementName"]).click()
        print("Accessing " + get_data["EngagementName"] + " engagement")

        # close the engagement introduction
        engagements = Engagement(self.driver)
        engagements.engagement_introduction_close_btn().click()
        time.sleep(2)

        if self.driver.find_element_by_xpath("//*[@id='dialogBox']/a").is_displayed():
            self.driver.find_element_by_xpath("//*[@id='dialogBox']/a").click()

        # user cannot access locked request
        engagements_method = EngagementMethods(self.driver)
        engagements_method.validate_user_cannot_view_locked_request()
        time.sleep(.5)

        # assign request to user
        self.driver.find_element_by_xpath("//span[normalize-space()='" + self.requestName2 + "']").click()
        engagements.add_assignment_btn().click()
        engagements.add_user_assignment(get_data["ClientAdminEmailFinal"]).click()
        print(get_data["ClientAdminEmailFinal"] + " was assigned to request" + self.requestName2)

        # download firm provided files
        engagements_method.download_firm_files_for_second_request()

        # lock request
        engagements_method.lock_the_request()

        # add comment to request
        engagements_method.add_new_comment_to_request("Add comment to request as Client Admin user")

        # upload files to request
        uploads = UploadsDownloads(self.driver)
        uploads.upload_file_rlm(uploads.op_ivy_pdf())
        uploads.upload_file_rlm(uploads.file_csv())
        uploads.upload_file_rlm(uploads.suralinkd_png())

        # change request status
        engagements_method.change_request_state_outstanding()

        # export request
        engagements_method.export_request()

        # export engagement
        engagements_method.export_engagement()

        # find previously created report
        clients_tab.clients_tab().click()
        clients_list.select_report_lnk().click()

        # invite guest user to report
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
        report_method = ReportMethods(self.driver)
        set_guest_email = get_data["ClientEmail"] + "{}".format(
            datetime.now().strftime("%m%d%Y-%H%M%S")) + "@suralink.com"
        time.sleep(2)
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")

        report_method.invite_guestUser_individual("GuestFirst", "GuestLast", set_guest_email)

        # download file from report
        report_method.download_file_from_report()

        # Create new client user
        clients_method = ClientMethods(self.driver)
        set_client_email = get_data["ClientEmail"] + "{}".format(
            datetime.now().strftime("%m%d%Y-%H%M%S")) + "@suralink.com"
        clients_method.invite_client_user("ClientFirst2", "ClientLast2", set_client_email)

    @pytest.fixture(params=Data.get_data("Smoke Test"))
    def get_data(self, request):
        return request.param
