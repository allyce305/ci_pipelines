
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

driver = None


def test_pipeline():
    global driver

    options = webdriver.ChromeOptions()
    options.add_argument('--incognito')
    # path = os.path.join(os.path.join(os.environ['USERPROFILE']), get_data["BrowserLocation"])
    # driver = webdriver.Chrome(executable_path=path, options=options)
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)
    driver.get("https://stage.suralink.com")
    driver.maximize_window()
    driver.implicitly_wait(10)

    print("This is the web driver ++++++++++++++++++++++++++++++++++")
    driver.quit()
