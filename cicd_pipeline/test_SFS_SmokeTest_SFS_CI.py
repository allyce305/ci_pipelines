import time
from datetime import datetime
import pytest
import utilities
from functions.CommonFeatures import CommonMethods
from functions.LoginFeatures import LoginMethods
from functions.SFSFeatures import SFSMethods
from pageObjects.SFSTab import SecureFilesSharingTab
from pageObjects.SFS_Profile import SFSProfile
from utilities import Data, gmailAPI
from utilities.fileUploadDownload import UploadsDownloads


# SFS must be enabled for this test
# User will need to manually validate email features

# Test requires data element from stored excel testDataLocation:
#                   login email = getData["FirmUserEmail"]
#                   user login password = getData["FirmUserPassword"])

@pytest.mark.Pipeline
@pytest.mark.SFS
@pytest.mark.SmokeTest
@pytest.mark.hookwrapper("pytest_runtest_makereport")
@pytest.mark.usefixtures("setup_method", scope="class")
class Test:
    SubjectLine = "Subject {}".format(datetime.now().strftime("%m/%d/%Y %H:%M:%S"))

    def test_regression_sfs_send_file(self, get_data):
        # Firm user login
        login_method = LoginMethods(self.driver)
        login_method.valid_login(get_data["FirmUserEmail"], get_data["FirmUserPassword"])

        # send file
        sfs_tab = SecureFilesSharingTab(self.driver)
        sfs_tab.secure_file_sharing_tab().click()
        sfs_profile = SFSProfile(self.driver)
        sfs = SFSMethods(self.driver)
        uploads = UploadsDownloads(self.driver)

        sfs_profile.open_drawers_btn().click()
        set_Subject = self.SubjectLine
        sfs.send_files_in_sfs(uploads.op_ivy_pdf(), "Recipient Name", get_data["FirmUserEmail"], set_Subject)
        time.sleep(5)

        #        # validate message is in user email inbox
        gmail = utilities.gmailAPI
        results = gmail.searchMessages(gmail.service, "has shared documents with you! Click on the link below to "
                                                      "access your files.")

        for msg in results:
            message = gmail.readMessage(gmail.service, msg)

            print("Message: " + str(message))

        # request files
        sfs_profile.request_files_drawer_tab().click()
        sfs_profile.request_file_recipient_email_txt().send_keys(get_data["FirmUserEmail"])
        sfs_profile.request_file_message_txt().send_keys("Add a personalized message or instructions")
        time.sleep(3)
        sfs_profile.send_email_btn().click()

        common = CommonMethods(self.driver)
        assert common.expected_text("Request emails sent to")
        print("File request for " + get_data["FirmUserEmail"])

        # compose
        sfs_profile = SFSProfile(self.driver)
        sfs_profile.my_link_drawer_tab().click()
        link1 = sfs_profile.compose_link_url().get_attribute('value')
        print("Compose Link: " + link1)
        time.sleep(1)

        # navigate to Compose link
        self.driver.get(link1)
        time.sleep(1)
        assert common.find_element_by_normalize_space2("Upload a file or two to get started")
        print("User was able to navigate to compose link")

    @pytest.fixture(params=Data.get_data("Smoke Test"))
    def get_data(self, request):
        return request.param
