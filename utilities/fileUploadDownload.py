import os
import shutil
import time
from selenium.webdriver.common.by import By


class UploadsDownloads:

    def __init__(self, driver):
        self.driver = driver

    download_path = os.path.join(os.path.join(os.environ['USERPROFILE']), "Downloads")
    RepoDirectoryPath = os.path.normpath(os.path.dirname(os.path.abspath(__file__)) + "/..")
    RLM_UploadFile = (By.XPATH, "//input[@class = 'dz-hidden-input']")
    RLM_UploadPhoto = (By.XPATH, "//input[@id='uploadPhoto']")
    RLM_UploadProfilePhoto = (By.XPATH, "//input[@class='_2Y6QsytFkIen8EZ8qRminO']")
    SFS_UploadFile = (By.XPATH, "(//div[@class='FileUpload_dropboxContent__2LF1P'])[1]")

    def set_rlm_photo_for_upload(self):
        return self.driver.find_element(*UploadsDownloads.RLM_UploadPhoto)

    def set_sfs_file_for_upload(self):
        return self.driver.find_element(*UploadsDownloads.SFS_UploadFile)

    def upload_profile_photo(self):
        return self.driver.find_element(*UploadsDownloads.RLM_UploadProfilePhoto)

    # get the most recent file in folder location and change name to known name
    def get_most_recent_file(self, file_name):
        initial_path = self.download_path
        filename_temp = max([initial_path + "/" + f for f in os.listdir(initial_path)], key=os.path.getctime)
        change_name = shutil.move(filename_temp, os.path.join(initial_path, file_name))
        print("File Download Location: " + change_name)
        return change_name

    # search for known file and validate file is found
    def validate_file_download(self, file_name):

        self.get_most_recent_file(file_name)

        stop = 15
        counter = 0
        while not os.path.exists(self.download_path + "/" + file_name):
            time.sleep(1)
            counter += 1
            if counter > stop:
                break

        if os.path.isfile(self.download_path + "/" + file_name):
            print("File was downloaded to default location")
        else:
            raise ValueError("%s isn't a file!" % self.download_path)

    def upload_file_sfs(self, file):
        uploads = UploadsDownloads(self.driver)
        uploads.set_sfs_file_for_upload().send_keys(file)
        time.sleep(3)

    def upload_file_rlm(self, file):
        uploads = UploadsDownloads(self.driver)
        uploads.set_rlm_file_for_upload().send_keys(file)
        # time.sleep(3)

    def upload_photo_rlm(self, file):
        uploads = UploadsDownloads(self.driver)
        uploads.set_rlm_photo_for_upload().send_keys(file)
        time.sleep(3)

    def upload_2_files_rlm(self, file1, file2):
        uploads = UploadsDownloads(self.driver)
        uploads.set_rlm_file_for_upload().send_keys(file1, file2)
        time.sleep(5)

    def set_rlm_file_for_upload(self):
        return self.driver.find_element(*UploadsDownloads.RLM_UploadFile)

    def face_to_face_pdf(self):
        face_to_face_pdf_file_path = self.RepoDirectoryPath + "/uploadFiles/facetoface.pdf"
        return face_to_face_pdf_file_path

    def sample_tax_entity_xlsx(self):
        sample_enagement_file_path = self.RepoDirectoryPath + "/uploadFiles/Sample_Tax_Entity.xlsx"
        return sample_enagement_file_path

    def op_ivy_pdf(self):
        op_ivy_file_path = self.RepoDirectoryPath + "/uploadFiles/opIvy.pdf"
        return op_ivy_file_path

    def report_file_pdf(self):
        report_file_pdf = self.RepoDirectoryPath + "/uploadFiles/reportFile.pdf"
        return report_file_pdf

    def file_csv(self):
        csv_file_sheet = self.RepoDirectoryPath + "/uploadFiles/CSVFile - Sheet1.csv"
        return csv_file_sheet

    def suralinkd_png(self):
        suralinkd = self.RepoDirectoryPath + "/uploadFiles/suralinkd.png"
        return suralinkd

    def personal_profile_photo(self):
        profile_photo = self.RepoDirectoryPath + "/uploadFiles/profilePhoto.png"
        return profile_photo

    def bin_file(self):
        bin_file2 = self.RepoDirectoryPath + "/uploadFiles/BINFile.bin"
        return bin_file2

    def doc_file(self):
        doc = self.RepoDirectoryPath + "/uploadFiles/DocFile.docx"
        return doc

    def epub_file(self):
        epub = self.RepoDirectoryPath + "/uploadFiles/EPUBFormat.epub"
        return epub

    def html_file(self):
        html = self.RepoDirectoryPath + "/uploadFiles/HTMLFile.html"
        return html

    def jpeg_file(self):
        jpeg = self.RepoDirectoryPath + "/uploadFiles/JPEG.jpg"
        return jpeg

    def ods_file(self):
        ods = self.RepoDirectoryPath + "/uploadFiles/OpenDocFile.ods"
        return ods

    def password_protected(self):
        password = self.RepoDirectoryPath + "/uploadFiles/PasswordProtectedPDF.pdf"
        return password

    def plain_text_format(self):
        txt = self.RepoDirectoryPath + "/uploadFiles/PlaneTextFormat.txt"
        return txt

    def png_file(self):
        png = self.RepoDirectoryPath + "/uploadFiles/PNGFile.png"
        return png

    def rich_text_format(self):
        rtf = self.RepoDirectoryPath + "/uploadFiles/RichTextFormat.rtf"
        return rtf

    def text_file1(self):
        txt2 = self.RepoDirectoryPath + "/uploadFiles/TextFile1.txt"
        return txt2

    def text_file2(self):
        txt3 = self.RepoDirectoryPath + "/uploadFiles/TextFile2.txt"
        return txt3

    def text_file3(self):
        txt4 = self.RepoDirectoryPath + "/uploadFiles/TextFile3.txt"
        return txt4

    def text_file4(self):
        txt5 = self.RepoDirectoryPath + "/uploadFiles/TextFile4.txt"
        return txt5

    def text_file5(self):
        txt6 = self.RepoDirectoryPath + "/uploadFiles/TextFile5.txt"
        return txt6

    def tsv_file(self):
        tsv = self.RepoDirectoryPath + "/uploadFiles/TSVFile.tsv"
        return tsv

    def zip_file(self):
        zip_file = self.RepoDirectoryPath + "/uploadFiles/webPageFile.zip"
        return zip_file
