import os
import openpyxl


class Excel:
    testDataLocation = "Desktop\\TestData.xlsx"
    path = os.path.join(os.path.join(os.environ['USERPROFILE']), testDataLocation)


def get_data(test_script_name):
    info = {}
    book = openpyxl.load_workbook(Excel.path)
    sheet = book.get_sheet_by_name("TestData")
    for i in range(1, sheet.max_row + 1):  # to get rows
        if sheet.cell(row=i, column=1).value == test_script_name:

            for j in range(2, sheet.max_column + 1):  # to get columns
                info[sheet.cell(row=1, column=j).value] = sheet.cell(row=i, column=j).value
    return [info]


def set_test_data(cell_id, data):
    book = openpyxl.load_workbook(Excel.path)
    sheet = book.get_sheet_by_name("TestData")
    sheet[cell_id].value = data
    book.save(Excel.path)


def write_data_to_file(file_name, data_to_write):
    f = open(file_name, "w+")
    f.write(data_to_write)
    f.close()


def read_data_file(file_name):
    f = open(file_name)
    print(f.read())
    f.close()

    return print(f.read())
