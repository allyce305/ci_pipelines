import time

from pageObjects.Footer_MyAccount import MyAccount
from utilities.fileUploadDownload import UploadsDownloads


class MyAccountMethods:

    def __init__(self, driver):
        self.driver = driver

    def change_profile_first_last_name(self, first_name, last_name):
        my_account = MyAccount(self.driver)

        my_account.my_account_btn().click()
        my_account.personal_profile_tab().click()
        my_account.first_name_txt().clear()
        my_account.first_name_txt().send_keys(first_name)
        my_account.last_name_txt().clear()
        my_account.last_name_txt().send_keys(last_name)
        my_account.save_settings_btn().click()
        time.sleep(2)

    def change_profile_photo(self, profile_photo):
        my_account = MyAccount(self.driver)
        uploads = UploadsDownloads(self.driver)

        my_account.my_account_btn().click()
        my_account.personal_profile_tab().click()
        my_account.photo_btn().click()
        uploads.upload_profile_photo().send_keys(profile_photo)
        my_account.photo_save_btn().click()

    def change_timeout_session(self, minutes):
        session_settings = MyAccount(self.driver)
        session_settings.timeout_ddl().click()
        time.sleep(1)

        if minutes == 5:
            session_settings.timeout_5min_opt().click()
            print("Time out session is set to : " + str(minutes) + " minutes")
        elif minutes == 10:
            session_settings.timeout_10min_opt().click()
            print("Time out session is set to : " + str(minutes) + " minutes")
        elif minutes == 15:
            session_settings.timeout_15min_opt().click()
            print("Time out session is set to : " + str(minutes) + " minutes")
        elif minutes == 30:
            session_settings.timeout_30min_opt().click()
            print("Time out session is set to : " + str(minutes) + " minutes")
        elif minutes == 60:
            session_settings.timeout_1hr_opt().click()
            print("Time out session is set to : " + str(minutes / 60) + " hour")
        elif minutes == 180:
            session_settings.timeout_3hr_opt().click()
            print("Time out session is set to : " + str(minutes / 60) + " hours")
        elif minutes == 360:
            session_settings.timeout_6hr_opt().click()
            print("Time out session is set to : " + str(minutes / 60) + " hours")
        elif minutes == 720:
            session_settings.timeout_12hr_opt().click()
            print("Time out session is set to : " + str(minutes / 60) + " hours")
        elif minutes == 1440:
            session_settings.timeout_24hr_opt().click()
            print("Time out session is set to : " + str(minutes / 60) + " hours")
        else:
            print("Invalid selection")

        session_settings.save_settings_btn().click()
        assert session_settings.save_confirmation_txt()

    def return_users_timeout_setting_to_original_value(self, value):
        session_settings = MyAccount(self.driver)
        session_settings.timeout_ddl().click()
        time.sleep(1)

        if value == "5 minutes":
            session_settings.timeout_5min_opt().click()
            print("Time out session is set to : " + value)
        elif value == "10 minutes":
            session_settings.timeout_10min_opt().click()
            print("Time out session is set to : " + value)
        elif value == "15 minutes":
            session_settings.timeout_15min_opt().click()
            print("Time out session is set to : " + value)
        elif value == "30 minutes":
            session_settings.timeout_30min_opt().click()
            print("Time out session is set to : " + value)
        elif value == "1 hour":
            session_settings.timeout_1hr_opt().click()
            print("Time out session is set to : " + value)
        elif value == "3 hours":
            session_settings.timeout_3hr_opt().click()
            print("Time out session is set to : " + value)
        elif value == "6 hours":
            session_settings.timeout_6hr_opt().click()
            print("Time out session is set to : " + value)
        elif value == "12 hours":
            session_settings.timeout_12hr_opt().click()
            print("Time out session is set to : " + value)
        elif value == "24 hours":
            session_settings.timeout_24hr_opt().click()
            print("Time out session is set to : " + value)
        else:
            print("Invalid selection")

        session_settings.save_settings_btn().click()
        assert session_settings.save_confirmation_txt()

    @staticmethod
    def countdown(time_to_countdown):
        time_countdown = (time_to_countdown * 60) - 20

        while time_countdown:
            mins, secs = divmod(time_countdown, 60)
            timer = '{:02d}:{:02d}'.format(mins, secs)
            print(timer, end="\r")
            time.sleep(1)
            time_countdown -= 1
        print("Countdown is completed. Time is up!!!")
