import time

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from pageObjects.ClientsTab import ClientTab
from pageObjects.Clients_ClientsList import ClientsList
from pageObjects.EngagementsTab import Engagement
from utilities.fileUploadDownload import UploadsDownloads


class EngagementMethods:

    def __init__(self, driver):
        self.driver = driver

    def setup_manual_engagement_for_smoke(self, category_name, request_name1, user_name, request_name2, comment):
        engagements = Engagement(self.driver)

        # add category to engagement
        engagements.create_category_btn().click()
        engagements.category_name_txt().send_keys(category_name)
        engagements.create_new_category_btn().click()
        # add first request to category
        engagements.create_request_btn().click()
        engagements.request_name_txt().send_keys(request_name1)
        engagements.create_new_request_btn().click()
        # assign user to request
        engagements.add_assignment_btn().click()
        engagements.add_user_assignment(user_name).click()
        # lock request assigned to firm user
        engagements.request_more_options_icn().click()
        engagements.lock_request_btn().click()
        string = engagements.request_locked_title().text
        assert string == "Request is Locked"
        # add second request to category
        engagements.create_request_btn().click()
        engagements.request_name_txt().send_keys(request_name2)
        engagements.create_new_request_btn().click()
        # add comment as a firm user
        engagements.add_comment_btn().send_keys(comment)
        engagements.send_comment_btn().click()
        # upload file to request
        uploads = UploadsDownloads(self.driver)
        uploads.upload_file_rlm(uploads.op_ivy_pdf())
        time.sleep(2)

    def setup_manual_engagement_for_regression(self, category_name, request_name, user_name, comment):
        engagements = Engagement(self.driver)

        # add category to engagement
        engagements.create_category_btn().click()
        engagements.category_name_txt().send_keys(category_name)
        engagements.create_new_category_btn().click()
        # add first request to category
        engagements.create_request_btn().click()
        engagements.request_name_txt().send_keys(request_name)
        engagements.create_new_request_btn().click()
        # assign user to request
        engagements.add_assignment_btn().click()
        engagements.add_user_assignment(user_name).click()
        # lock request assigned to firm user
        engagements.request_more_options_icn().click()
        engagements.lock_request_btn().click()
        string = engagements.request_locked_title().text
        assert string == "Request is Locked"
        # add comment as a firm user
        engagements.add_comment_btn().send_keys(comment)
        engagements.send_comment_btn().click()
        # upload file to request
        uploads = UploadsDownloads(self.driver)
        uploads.upload_file_rlm(uploads.op_ivy_pdf())

    def create_category_for_engagement(self, categoryName):
        engagements = Engagement(self.driver)

        engagements.create_category_btn().click()
        engagements.category_name_txt().send_keys(categoryName)
        engagements.create_new_category_btn().click()

        assert self.driver.find_element_by_xpath("//span[@title='" + categoryName + "']")
        print(categoryName + " was created sucessfully")

    def create_request_for_engagement(self, requestName):
        engagements = Engagement(self.driver)

        engagements.create_request_btn().click()
        engagements.request_name_txt().send_keys(requestName)
        engagements.create_new_request_btn().click()

        assert self.driver.find_element_by_xpath("//span[normalize-space()='" + requestName + "']")
        print(requestName + " was created sucessfully")

    def create_3_requests_for_engagement(self, engagement_name, request_name):
        # method adds a request for the most recent (first listed) category
        clients_tab = ClientTab(self.driver)
        engagements = Engagement(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_list_tab().click()

        time.sleep(3)
        clients_list.select_engagement_lnk(engagement_name).click()
        engagements.create_request_btn().click()
        engagements.request_name_txt().send_keys(request_name + " #1")
        engagements.create_another_request_btn().click()
        engagements.request_name_txt().send_keys(request_name + " #2")
        engagements.create_another_request_btn().click()
        engagements.request_name_txt().send_keys(request_name + " #3")
        engagements.create_new_request_btn().click()

    def assign_user_to_request(self, userName):
        engagements = Engagement(self.driver)

        engagements.add_assignment_btn().click()
        engagements.add_user_assignment(userName).click()
        time.sleep(1)

        assert self.driver.find_element_by_xpath("//span[normalize-space()='Added User Assignment']")
        print(userName + " was assigned to request")

    def lock_the_request(self):
        engagements = Engagement(self.driver)
        engagements.request_more_options_icn().click()
        engagements.lock_request_btn().click()
        string = engagements.request_locked_title().text
        assert string == "Request is Locked"
        print("Request has been locked")

    def export_request(self):
        engagements = Engagement(self.driver)
        engagements.request_more_options_icn().click()
        engagements.export_print_request_btn().click()
        engagements.export_as_excel_btn().click()
        time.sleep(1)
        engagements.close_exporter_btn().click()

    def export_engagement(self):
        engagements = Engagement(self.driver)
        engagements.more_audit_options_icn().click()
        engagements.export_print_engagement_btn().click()
        engagements.export_as_excel_btn().click()
        time.sleep(1)
        engagements.close_exporter_btn().click()

    def add_new_comment_to_request(self, text):
        engagements = Engagement(self.driver)

        engagements.add_comment_btn().send_keys(text)
        engagements.send_comment_btn().click()

        assert self.driver.find_element_by_xpath(
            "(//span[@stye='position:relative; display:block;'][normalize-space()='" + text + "'])[1]")
        print(text + " was added as a comment")

    def upload_file_to_request(self, file):
        uploads = UploadsDownloads(self.driver)
        uploads.set_rlm_file_for_upload().send_keys(file)
        time.sleep(3)

    def download_firm_files_for_second_request(self):
        engagements = Engagement(self.driver)
        # engagements.firm_provided_file_lnk().click()
        self.driver.find_element_by_xpath("(//span[contains(text(),'Firm provided')])[2]").click()

        assert WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located((By.XPATH, "(//span[contains(text(),'Firm Files')])")))
        engagements.firm_files_check_all_chk().click()
        engagements.firm_files_download_selected_files_btn().click()
        time.sleep(2)
        engagements.firm_files_close_btn().click()

    def validate_user_cannot_view_locked_request(self):
        time.sleep(2)
        engagements = Engagement(self.driver)
        engagements.request_locked().click()

        actions = ActionChains(self.driver)
        actions.move_to_element(engagements.request_locked())
        actions.click_and_hold(engagements.request_locked())
        actions.pause(3)
        assert WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located((By.XPATH, "//div[@class='lockBox']")))
        actions.reset_actions()
        print("User cannot view locked file")

    def change_request_state_outstanding(self):
        engagements = Engagement(self.driver)

        actions = ActionChains(self.driver)
        engagements.change_request_state_ddl().click()
        actions.move_to_element(engagements.change_request_state_ddl())
        actions.click_and_hold(engagements.change_request_state_ddl())
        actions.pause(2)
        actions.move_to_element(engagements.change_state_outstanding_ddl())
        actions.click(engagements.change_state_outstanding_ddl())
        actions.pause(2)
        WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located((By.LINK_TEXT, "Mark as Outstanding")))
        actions.reset_actions()
        engagements.change_state_outstanding_ddl().click()
