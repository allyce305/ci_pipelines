import time

from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec

from pageObjects.LiveChatSupportPage import LiveChatSupport


class ChatSupportMethods:

    def __init__(self, driver):
        self.driver = driver

    def open_chat_support_window(self):
        chat_support = LiveChatSupport(self.driver)
        original_window = self.driver.current_window_handle
        assert len(self.driver.window_handles) == 1
        chat_support.live_chat_support_btn().click()
        WebDriverWait(self.driver, 20).until(ec.number_of_windows_to_be(2))

        for window_handle in self.driver.window_handles:
            if window_handle != original_window:
                self.driver.switch_to.window(window_handle)
                break

        WebDriverWait(self.driver, 20).until(ec.title_is("Suralink Support"))

        time.sleep(2)
        self.driver.close()
        self.driver.switch_to.window(original_window)
        time.sleep(2)
