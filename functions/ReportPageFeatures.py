import time
from selenium.common.exceptions import NoSuchElementException
from functions.CommonFeatures import CommonMethods
from pageObjects.ReportPage import Report


class ReportMethods:

    def __init__(self, driver):
        self.driver = driver

    def invite_guestUser_individual(self, user_first_name, user_last_name, client_user_email):
        report = Report(self.driver)

        time.sleep(2)
        report.invite_guest_user_btn().click()
        time.sleep(1)
        report.first_name_txt().send_keys(user_first_name)
        report.last_name_txt().send_keys(user_last_name)
        report.email_txt().send_keys(client_user_email)
        report.next_access_details_btn().click()
        time.sleep(1)
        report.duration_length_chk().click()
        report.next_file_assignments_btn().click()
        time.sleep(1)
        report.file_access_invite_guest_users_btn().click()
        try:
            if self.driver.find_element_by_link_text(client_user_email).is_displayed():
                print(client_user_email + " has been invited to your report as a guest user")
                assert self.driver.find_element_by_link_text(client_user_email)
            elif self.driver.find_element_by_partial_link_text(client_user_email).is_displayed():
                print(client_user_email + " has been invited to your report as a guest user")
                assert self.driver.find_element_by_partial_link_text(client_user_email)
            else:
                assert self.driver.find_element_by_partial_link_text(client_user_email)
                print(client_user_email + " has not been invited to your report as a guest user")
        except NoSuchElementException:
            print("Failed to invite guest user " + client_user_email)

    def download_file_from_report(self):
        report = Report(self.driver)
        common = CommonMethods(self.driver)

        report.download_files_chk().click()
        time.sleep(1)
        report.download_files_btn().click()
        assert common.expected_text("Starting download...")
        print("Downloading report....")
