from selenium.webdriver.common.by import By
import time


class CommonMethods:
    def __init__(self, driver):
        self.driver = driver

    YesNo_No_button = (By.XPATH, "//*[@id='yesNo_NoBtn']")
    YesNo_Yes_button = (By.XPATH, "//*[@id='yesNo_YesBtn']")
    ConfirmDelete_input = (By.XPATH, "//*[@id='yesNo_twoStageInput']")

    def find_element_by_normalize_space(self, text):
        return self.driver.find_element_by_xpath("//p[normalize-space()='" + text + "']")

    def find_element_by_normalize_space2(self, text):
        return self.driver.find_element_by_xpath('//*[normalize-space()="' + text + '"]')

    def find_element_by_normalize_space3(self, text):
        return self.driver.find_element_by_xpath("//*[normalize-space()='" + text + "']")

    def confirm_delete_input(self):
        time.sleep(1)
        return self.driver.find_element(*CommonMethods.ConfirmDelete_input)

    def yesno_yes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*CommonMethods.YesNo_Yes_button)

    def yesno_no_btn(self):
        time.sleep(1)
        return self.driver.find_element(*CommonMethods.YesNo_No_button)

    # use if text is surrounded by single quotes
    def expected_text(self, text):
        return self.driver.find_element_by_xpath("//*[contains(text(), '" + text + "')]")

    # use if text is surrounded by double quotes
    def expected_text_2(self, text):
        return self.driver.find_element_by_xpath('//*[contains(text(), "' + text + '")]')
