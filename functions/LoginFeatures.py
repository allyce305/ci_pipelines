import time

from pageObjects.FooterPage import Footer
from pageObjects.LoginPage import Login


class LoginMethods:

    def __init__(self, driver):
        self.driver = driver

    def valid_login(self, email, password):
        login = Login(self.driver)
        login.user_email_txt().send_keys(email)
        login.user_password_txt().send_keys(password)
        try:
            login.cookies_btn().click()
        except:
            print("Cookie notification not found")
        time.sleep(1)
        login.signin_btn().click()
        time.sleep(6)

        # validate correct user is logged in
        footer = Footer(self.driver)
        found = footer.logged_in_as_user().text
        x = found.split(" - ")
        assert str(x[1]).find(str(email)) != -1
        print(email + " user was logged in successfully")

    def invalid_email(self, email, password, expected_edit_message):

        login = Login(self.driver)
        login.user_email_txt().send_keys(email + "x")
        login.user_password_txt().send_keys(password)
        login.signin_btn().click()
        time.sleep(6)

        # validate user edit message displays on the UI
        assert login.expected_text(expected_edit_message).is_displayed()
        print(email + " user was not logged")

    def invalid_password(self, email, password, expected_edit_message):
        login = Login(self.driver)
        login.user_email_txt().send_keys(email)
        login.user_password_txt().send_keys(password + "x")
        login.signin_btn().click()
        time.sleep(6)

        # validate user edit message displays on the UI
        assert login.expected_text(expected_edit_message).is_displayed()
        print(email + " user was not logged")
