import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec

from functions.CommonFeatures import CommonMethods
from pageObjects.ClientsTab import ClientTab
from pageObjects.Clients_ClientsList import ClientsList
from pageObjects.EngagementsTab import Engagement
from utilities.fileUploadDownload import UploadsDownloads


class ClientMethods:

    def __init__(self, driver):
        self.driver = driver

    def create_client_pipeline(self, clientName, clientId):
        clients_list = ClientsList(self.driver)
        clients_tab = ClientTab(self.driver)

        clients_tab.clients_tab().click()
        # clients_tab.clients_list_tab().click()
        clients_list.create_client_btn().click()
        clients_list.client_name_txt().send_keys(clientName)
        clients_list.custom_client_id_number_txt().send_keys(clientId)
        clients_list.create_new_client_btn().click()

        assert self.driver.find_element_by_xpath("//div[@title='" + clientName + "']")
        print(clientName + " was created successfully")

    def create_client(self, clientName, clientId):
        clients_list = ClientsList(self.driver)
        clients_tab = ClientTab(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_list_tab().click()
        clients_list.create_client_btn().click()
        clients_list.client_name_txt().send_keys(clientName)
        clients_list.custom_client_id_number_txt().send_keys(clientId)
        clients_list.create_new_client_btn().click()
        time.sleep(0.5)
        self.driver.refresh()

        assert self.driver.find_element_by_xpath("//div[@title='" + clientName + "']")
        print(clientName + " was created successfully")

    def delete_client(self):
        clients_list = ClientsList(self.driver)
        clients_tab = ClientTab(self.driver)
        common = CommonMethods(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_list_tab().click()
        clients_list.delete_client_btn().click()
        common.yesno_yes_btn().click()
        common.confirm_delete_input().send_keys("yesdelete")
        common.yesno_yes_btn().click()
        assert self.driver.find_element_by_xpath("//*[@id='messageBox-text']")

        clients_list.close_alert_btn().click()

    def create_new_manual_engagement(self, engagement_name, engagement_due_date):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.create_new_client_engagement_btn().click()
        clients_list.engagement_name_txt().send_keys(engagement_name)
        clients_list.new_engagement_due_date_txt().send_keys(engagement_due_date + Keys.ENTER)
        clients_list.create_new_engagement_btn().click()
        time.sleep(2)

        engagements = Engagement(self.driver)
        assert engagements.create_request_btn()

        print(engagement_name + " was created successfully")

    def create_new_import_engagement(self, engagementName, file):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)
        uploads = UploadsDownloads(self.driver)

        clients_tab.clients_tab().click()
        clients_list.create_new_client_engagement_btn().click()
        clients_list.engagement_name_txt().send_keys(engagementName)
        clients_list.import_engagement_from_excel_chk().click()
        uploads.upload_file_rlm(file)
        WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((
            By.XPATH, "//div[@id='importExcelDropZoneArea' and @class='pointerButton']")))
        clients_list.create_new_engagement_btn().click()

        assert clients_list.import_categories_chk()
        print(engagementName + " was created successfully")

        time.sleep(2)
        clients_list.import_categories_chk().click()
        clients_list.identify_category_import_engagement().click()
        clients_list.identify_subcategory_import_engagement().click()
        clients_list.identify_request_name_import_engagement().click()
        clients_list.identify_request_description_import_engagement().click()
        clients_list.doesnt_apply_to_document_btn().click()
        clients_list.create_import_engagement_btn().click()

        assert WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((
            By.XPATH, "//a[@id='buttonBar_newCat']")))
        print("Import configuration complete")

    def create_new_clone_engagement(self, clone_engagement_name, engagement_name):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.create_new_client_engagement_btn().click()
        clients_list.engagement_name_txt().send_keys(clone_engagement_name)
        clients_list.clone_existing_engagement_chk().click()
        clients_list.input_engagement_for_clone().send_keys(engagement_name)
        WebDriverWait(self.driver, 10).until(clients_list.search_for_engagement_for_clone_text()).click()
        clients_list.include_firm_provided_files_chk().click()
        clients_list.create_new_engagement_btn().click()

        assert WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((
            By.XPATH, "//a[@id='buttonBar_newCat']")))
        print(clone_engagement_name + " was created successfully")

    def create_new_report(self, report_name, file):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)
        uploads = UploadsDownloads(self.driver)

        clients_tab.clients_tab().click()
        clients_list.publish_new_report_btn().click()
        clients_list.report_name_txt().send_keys(report_name)
        # Added sleep to stop script from interrupting file upload
        time.sleep(1)
        uploads.upload_file_rlm(file)
        # Added sleep to stop script from interrupting file upload
        time.sleep(1)
        WebDriverWait(self.driver, 10).until(clients_list.select_users_report_btn()).click()
        WebDriverWait(self.driver, 10).until(clients_list.publish_report_btn()).click()

        assert WebDriverWait(self.driver, 10).until(ec.visibility_of_element_located((
            By.XPATH, "//h4[normalize-space()='Prepared For:']")))
        print(report_name + "was successfully created")

    def invite_client_admin_user(self, user_first_name, user_last_name, client_user_email):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.invite_client_user_btn().click()
        clients_list.client_user_first_name_txt().send_keys(user_first_name)
        clients_list.client_user_last_name_txt().send_keys(user_last_name)
        clients_list.client_user_email_address_txt().send_keys(client_user_email)
        clients_list.make_client_admin_chk().click()
        clients_list.send_invitation_btn().click()

        #assert self.driver.find_element_by_link_text(client_user_email)
        print(client_user_email + "has been invited to your firm")

    def invite_client_user(self, user_first_name, user_last_name, client_user_email):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.invite_client_user_btn().click()
        clients_list.client_user_first_name_txt().send_keys(user_first_name)
        clients_list.client_user_last_name_txt().send_keys(user_last_name)
        clients_list.client_user_email_address_txt().send_keys(client_user_email)
        clients_list.send_invitation_btn().click()

        assert self.driver.find_element_by_link_text(client_user_email)
        print(client_user_email + " has been invited to your firm")

    def send_status_update(self):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)
        common = CommonMethods(self.driver)

        clients_tab.clients_tab().click()
        clients_list.status_update_btn().click()
        clients_list.status_update_select_all_chk().click()
        clients_list.next_select_specific_users_btn().click()

        try:
            common.find_element_by_normalize_space3("You must select some clients and engagements!")
        except:
            print("You do not have any client users to send a Status update to.")
            clients_list.close_link_icon().click()
            print("Cannot send a Status Report")

        try:
            clients_list.send_email_btn().click()
            assert common.expected_text("Success")
            clients_list.close_alert_btn().click()
        except:
            print("Cannot send a status update")

    def export_clients_data(self):
        clients_tab = ClientTab(self.driver)
        clients_list = ClientsList(self.driver)

        clients_tab.clients_tab().click()
        clients_list.export_firm_btn().click()
        clients_list.export_data_btn().click()

    def get_SecureInvitaionLink_txt(self):
        client_list = ClientsList(self.driver)
        client_list.pending_invitation_get_link_btn().click()
        invitation_link = client_list.secure_invitation_link_txt().text

        print("Secure Invitation Link: " + invitation_link)
        client_list.secure_invitation_link_close_alert_btn().click()

        return invitation_link
