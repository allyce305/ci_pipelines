import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains

from pageObjects.ClientsTab import ClientTab
from pageObjects.Clients_ClientsDashboard import ClientsDashboard
from pageObjects.ManageDashboard import ManageClientsDashboard


class ManageDashboardMethods:

    def __init__(self, driver):
        self.driver = driver

    def create_static_list(self, list_name, criteria, filter_type, specifications):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        manage_dashboard.new_list_btn().click()
        manage_dashboard.list_name_txt().send_keys(list_name)
        manage_dashboard.criteria_ddl().send_keys(criteria + Keys.ENTER)
        manage_dashboard.filter_ddl().send_keys(filter_type + Keys.ENTER)
        manage_dashboard.specifications_ddl().send_keys(specifications + Keys.ENTER)
        manage_dashboard.add_btn().click()
        time.sleep(1)
        manage_dashboard.include_engagements_btn().click()
        manage_dashboard.save_list_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()

    def edit_static_list(self, edit):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        # manage_dashboard.static_list().click()
        manage_dashboard.edit_btn().click()
        manage_dashboard.description_txt().send_keys(edit)
        manage_dashboard.save_changes_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()

    def create_dynamic_list(self, list_name, list_type, criteria, filter_type, specifications):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        manage_dashboard.new_list_btn().click()
        manage_dashboard.list_name_txt().send_keys(list_name)
        manage_dashboard.list_type_ddl().send_keys(list_type + Keys.ENTER)
        manage_dashboard.criteria_ddl().send_keys(criteria + Keys.ENTER)
        manage_dashboard.filter_ddl().send_keys(filter_type + Keys.ENTER)
        manage_dashboard.specifications_ddl().send_keys(specifications + Keys.ENTER)
        manage_dashboard.add_btn().click()
        time.sleep(1)
        manage_dashboard.save_list_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()
        # Current bug doesn't close window with a single click after creating a dynamic list
        # time.sleep(1)
        # manage_dashboard.close_manage_dashboard_btn().click()

    def edit_dynamic_list(self, edit):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        # manage_dashboard.dynamic_list().click()
        manage_dashboard.edit_btn().click()
        manage_dashboard.description_txt().send_keys(edit)
        manage_dashboard.save_changes_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()

    def create_widget_set(self, set_name, description):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        manage_dashboard.widget_sets_tab().click()
        manage_dashboard.new_widget_sets_btn().click()
        manage_dashboard.widget_set_name_txt().send_keys(set_name)
        manage_dashboard.description_txt().send_keys(description)

        actions = ActionChains(self.driver)
        source = manage_dashboard.outstanding_requests_wgt()
        target = manage_dashboard.add_widgets_dzn()
        actions.drag_and_drop(source, target).perform()
        manage_dashboard.save_widget_set_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()

    def create_shared_list(self, list_name, criteria, filter_type, specifications):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        manage_dashboard.new_list_btn().click()
        manage_dashboard.list_name_txt().send_keys(list_name)
        manage_dashboard.criteria_ddl().send_keys(criteria + Keys.ENTER)
        manage_dashboard.filter_ddl().send_keys(filter_type + Keys.ENTER)
        manage_dashboard.specifications_ddl().send_keys(specifications + Keys.ENTER)
        manage_dashboard.add_btn().click()
        time.sleep(1)
        manage_dashboard.include_engagements_btn().click()
        manage_dashboard.shared_btn().click()
        manage_dashboard.save_list_btn().click()
        time.sleep(1)
        manage_dashboard.close_manage_dashboard_btn().click()

    def delete_list(self, list_name, criteria, filter_type, specifications):
        clients_tab = ClientTab(self.driver)
        clients_dashboard = ClientsDashboard(self.driver)
        manage_dashboard = ManageClientsDashboard(self.driver)

        clients_tab.clients_tab().click()
        clients_tab.clients_dashboard_tab().click()
        clients_dashboard.dashboard_loaded()
        clients_dashboard.manage_dashboard_btn().click()
        manage_dashboard.new_list_btn().click()
        manage_dashboard.list_name_txt().send_keys(list_name)
        manage_dashboard.criteria_ddl().send_keys(criteria + Keys.ENTER)
        manage_dashboard.filter_ddl().send_keys(filter_type + Keys.ENTER)
        manage_dashboard.specifications_ddl().send_keys(specifications + Keys.ENTER)
        manage_dashboard.add_btn().click()
        time.sleep(1)
        manage_dashboard.include_engagements_btn().click()
        manage_dashboard.save_list_btn().click()
        manage_dashboard.delete_btn().click()
        manage_dashboard.delete_yes_btn().click()
        time.sleep(2)
        manage_dashboard.close_manage_dashboard_btn().click()
