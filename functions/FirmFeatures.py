import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pageObjects.LoginPage import Login
from pageObjects.MyFirmTab import FirmTab
from utilities.fileUploadDownload import UploadsDownloads


class FirmMethods:

    def __init__(self, driver):
        self.driver = driver

    def enable_clientId(self):
        firm = FirmTab(self.driver)

        firm.enableRequiredClientId_button().click()
        firm.yesno_yes_btn().click()

        getText = self.driver.find_element(By.XPATH, "//div[normalize-space()='Success']")

        assert getText.text == "Success"
        print("Client Id required is enabled")

    def disable_clientId(self):
        firm = FirmTab(self.driver)

        firm.disableRequiredClientId_button().click()
        firm.yesno_yes_btn().click()
        time.sleep(1)

        getText = self.driver.find_element(By.XPATH, "//div[normalize-space()='Success']")

        assert getText.text == "Success"
        print("Client Id required is disabled")

    def enable_firmClientId(self):
        firm = FirmTab(self.driver)

        firm.my_firm_tab().click()
        try:
            firm.enableRequiredClientId_button().is_displayed()
            self.enable_clientId()
        except:
            self.disable_clientId()
            self.enable_clientId()

    def create_new_automation_rule(self, rule_name, action, description, criteria, apply_filter, specifications,
                                   timeframe, after, yes_delete):
        firm = FirmTab(self.driver)

        firm.my_firm_tab().click()
        firm.automation_tab().click()
        firm.create_new_rule_btn().click()
        firm.rule_name_txt().send_keys(rule_name)
        firm.action_ddl().send_keys(action + Keys.ENTER)
        firm.description_txt().send_keys(description)
        time.sleep(1)
        firm.criteria_ddl().send_keys(criteria + Keys.ENTER)
        firm.filter_ddl().send_keys(apply_filter + Keys.ENTER)
        firm.specifications_txt().send_keys(specifications + Keys.ENTER)
        time.sleep(1)
        firm.add_filter_btn().click()
        time.sleep(2)
        firm.timeframe_ddl().send_keys(timeframe + Keys.ENTER)
        firm.after_ddl().send_keys(after + Keys.ENTER)
        firm.yes_delete_continue_txt().send_keys(yes_delete)

        time.sleep(2)
        WebDriverWait(self.driver, 40).until(firm.create_rule_btn()).click()
        time.sleep(2)

        assert self.driver.find_element(By.XPATH,
                                        "//p[contains(text(),'Automatic deletions will apply to engagements wher')]")
        print(rule_name + " automation rule was created successfully")

    def enable_firm_eula(self, text):
        firm = FirmTab(self.driver)
        firm.my_firm_tab().click()

        firm = FirmTab(self.driver)
        # firm.company_tab().click()

        # check if EULA is enabled
        try:
            if firm.active_eula_icn().is_displayed():
                FirmMethods.disable_firm_eula(self)
        except NoSuchElementException:
            print("Firm EULA was not previously enabled")

        # Enable EULA
        firm.enable_eula_btn().click()
        firm.yesno_yes_btn().click()
        time.sleep(1)
        firm.eula_editor().send_keys(text)
        firm.save_agreement_btn().click()

        # validates that EULA is enabled
        assert firm.active_eula_icn().is_displayed()
        print("Firm EULA is enabled")

    def accept_eula(self, title, full_name, company):
        firm = FirmTab(self.driver)

        firm.view_eula_btn().click()
        firm.title_txt().send_keys(title)
        firm.full_name_txt().send_keys(full_name)
        firm.company_txt().send_keys(company)
        firm.agree_chk().click()
        firm.accept_contract_btn().click()
        firm.accept_yes_btn().click()
        assert self.driver.find_element(By.LINK_TEXT, "View EULA Contract")
        print("EULA Accepted")

    def activeFirmCoBranding_check(self):
        firm = FirmTab(self.driver)

        try:
            firm.enable_firm_co_branding_btn()
            print("Firm does not have co-branding enabled")
            return "true"
        except:
            print("Firm already has co-branding enabled")
            return "false"

    def enable_firm_co_branding(self, image):

        firm = FirmTab(self.driver)
        uploads = UploadsDownloads(self.driver)

        firm.my_firm_tab().click()

        if self.activeFirmCoBranding_check() == "true":
            firm.enable_firm_co_branding_btn().click()
            firm.yesno_yes_btn().click()
        elif self.activeFirmCoBranding_check() == "false":
            self.disable_firm_co_branding()
            firm.enable_firm_co_branding_btn().click()
            firm.yesno_yes_btn().click()
        else:
            login = Login(self.driver)
            print("Error found while trying to enable firm co-branding")
            assert login.cookies_btn()  # random error to kill test on failure

        time.sleep(2)
        uploads.set_rlm_photo_for_upload().send_keys(image)
        firm.save_cropped_image_btn().click()
        time.sleep(2)

        # validates that firm co-branding is enabled
        assert WebDriverWait(self.driver, 10).until(
            ec.presence_of_element_located(
                (By.XPATH, "//td[@class='navSubBar firmBranding company active']//i[contains(text(),'Active')]")))
        print("Firm Co-Branding is now enabled")

    def disable_firm_co_branding(self):
        firm = FirmTab(self.driver)

        time.sleep(1)
        firm.disable_firm_co_branding_btn().click()
        firm.yesno_yes_btn().click()
        print("Firm co-branding has been disabled")

    def disable_firm_eula(self):
        firm = FirmTab(self.driver)

        time.sleep(1)
        firm.my_firm_tab().click()

        # check if EULA is enabled
        try:
            if firm.active_eula_icn().is_displayed():
                firm.disable_eula_btn().click()
                firm.yesno_yes_btn().click()
                print("Firm EULA is disabled")

        except NoSuchElementException:
            print("Firm EULA was not previously enabled")

    def create_api_integration(self, application_name):
        firm = FirmTab(self.driver)

        firm.my_firm_tab().click()
        time.sleep(1)
        firm.integrations_tab().click()
        firm.connect_an_application_btn().click()
        firm.application_name_txt().send_keys(application_name)
        firm.accept_and_continue_btn().click()
