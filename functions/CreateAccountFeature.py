import time

from selenium.webdriver.common.by import By

from pageObjects.CreateAccountPage import CreateAccount


class CreateAccountMethods:

    def __init__(self, driver):
        self.driver = driver

    def create_account(self, url, password):
        self.driver.get(url)
        create_account = CreateAccount(self.driver)
        create_account.createPassword_input().send_keys(password)
        create_account.verifyPassWord_input().send_keys(password)
        create_account.understandResponsibilities_checkbox().click()
        create_account.createAccount_button().click()

        time.sleep(2)
        assert self.driver.find_element(By.XPATH,
                                        "//h1[normalize-space()='Welcome! We take your security seriously.']")
        print("New account was created successfully")
