import time

from functions.CommonFeatures import CommonMethods
from pageObjects.SFS_Profile import SFSProfile


class SFSMethods:

    def __init__(self, driver):
        self.driver = driver

    def send_files_in_sfs(self, upload_file, recipient_name, recipient_email, email_subject):
        sfs_profile = SFSProfile(self.driver)
        # uploads = UploadsDownloads(self.driver)

        sfs_profile.open_drawers_btn().click()

        if sfs_profile.drag_and_drop_file_dzn().is_displayed():
            sfs_profile.upload_file_drag_and_drop_txt().send_keys(upload_file)
            time.sleep(3)

        sfs_profile.who_are_you_sending_these_to_ddl().click()
        sfs_profile.send_files_recipient_name_txt().send_keys(recipient_name)
        sfs_profile.send_files_recipient_email_txt().send_keys(recipient_email)
        sfs_profile.subject_line_txt().send_keys(email_subject)
        sfs_profile.notes_txt().send_keys(
            "Put any information the person receiving your files might need to know here.")
        time.sleep(1)
        sfs_profile.send_link_to_view_files_btn().click()

        common = CommonMethods(self.driver)
        assert common.expected_text("To preview the message and files, click the Sent tab.")
        print("Message was sent to " + recipient_email)

    def request_files_in_sfs(self, recipient_email):
        sfs_profile = SFSProfile(self.driver)
        # sfs_profile.openDrawers_button().click()

        sfs_profile.request_files_drawer_tab().click()

        sfs_profile.send_files_recipient_email_txt().send_keys(recipient_email)
        sfs_profile.request_file_message_txt().send_keys("Add a personalized message or instructions")
        sfs_profile.send_email_btn().click()

        common = CommonMethods(self.driver)
        assert common.expected_text("Request emails sent to")
        print("Message request was sent to " + recipient_email)

    def regenerate_my_link(self):
        sfs_profile = SFSProfile(self.driver)
        # sfs_profile.openDrawers_button().click()

        sfs_profile.my_link_drawer_tab().click()
        link1 = sfs_profile.compose_link_url().get_attribute('value')
        print("Link #1 found: " + link1)
        sfs_profile.regenerate_link_btn().click()

        common = CommonMethods(self.driver)
        assert common.expected_text(
            "Once you regenerate your unique URL, everyone with the current link will lose access.")

        sfs_profile.yes_regenerate_my_link_now_btn().click()
        link2 = sfs_profile.compose_link_url().get_attribute('value')
        print("Link #2 found: " + link2)

        assert link1 != link2

    def navigate_to_compose_link(self):
        sfs_profile = SFSProfile(self.driver)
        # sfs_profile.openDrawers_button().click()

        sfs_profile.my_link_drawer_tab().click()
        link1 = sfs_profile.compose_link_url().get_attribute('value')
        print("Link: " + link1)

        return link1

    def delete_most_recent_message_on_sent(self, email_subject):
        sfs_profile = SFSProfile(self.driver)

        self.driver.refresh()  # page refresh used as new message can take up to 60 seconds to generate
        time.sleep(1)

        sfs_profile.sent_tab().click()
        sfs_profile.most_recent_sent_file().click()
        sfs_profile.sent_delete_most_recent_sent_file().click()
        sfs_profile.delete_message_btn().click()
        sfs_profile.yes_delete_all_file_btn().click()

        # validate message was deleted via UI
        common = CommonMethods(self.driver)
        assert common.expected_text('Message "' + email_subject + '" has been deleted')
        print('Message "' + email_subject + '" has been deleted')

    def restore_first_message_on_message_restore(self):
        sfs_profile = SFSProfile(self.driver)
        sfs_profile.restore_deleted_messages_files_lnk().click()
        time.sleep(1)

        # get first message on restore page
        target_message = sfs_profile.first_message_on_restore_message_list().text
        print("Message to Restore: " + target_message)

        sfs_profile.first_delete_icon_on_restore_message_list().click()
        time.sleep(2)

        target_message2 = sfs_profile.first_message_on_restore_message_list().text
        print("First Message Found on Restore Page: " + target_message2)

        # confirm message was removed from restore page
        assert target_message2 != target_message

        sfs_profile.close_message_restore_btn().click()
        time.sleep(1)
        self.driver.refresh()  # page refresh used as new message can take up to 60 seconds to generate
        time.sleep(2)
        self.driver.refresh()
        sfs_profile.sent_tab().click()

        # validate message was restored
        common = CommonMethods(self.driver)
        assert common.find_element_by_normalize_space(target_message)
        print("Expected Message " + target_message + " was found")
