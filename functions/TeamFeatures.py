import time
from selenium.webdriver import ActionChains
from functions.CommonFeatures import CommonMethods
from pageObjects.TeamTab import Teams


class TeamMethods:

    def __init__(self, driver):
        self.driver = driver

    def create_new_department(self, department_name):
        team = Teams(self.driver)

        team.team_tab().click()
        team.add_new_department_btn().click()
        team.department_name_txt().send_keys(department_name)
        team.confirm_add_new_department_btn().click()
        time.sleep(1)

        Common = CommonMethods(self.driver)
        assert Common.find_element_by_normalize_space3(department_name)
        print("Department was successfully created: " + department_name)

    def create_new_team_member_default_values(self, team_member_first_name, team_member_last_name, team_member_email):
        team = Teams(self.driver)

        team.team_tab().click()
        team.add_new_team_member_btn().click()
        team.team_member_first_name_txt().send_keys(team_member_first_name)
        team.team_member_last_name_txt().send_keys(team_member_last_name)
        team.team_member_email_txt().send_keys(team_member_email)
        team.confirm_add_new_team_member_button_txt().click()
        time.sleep(2)

    def getPendingInviteLink_text(self, URL, teamMemberEmail):
        common = CommonMethods(self.driver)

        common.find_element_by_normalize_space3(teamMemberEmail).click()
        target = self.driver.find_element_by_xpath("//*[contains(text(),'" + URL + "/auditors/views/Join.php?in')]")

        print("Pending Invite Secure Link: " + target.text)
        return target.text

    def deleteTeamMember(self, teamMemberEmail):
        teamMember = Teams(self.driver)
        common = CommonMethods(self.driver)

        common.find_element_by_normalize_space3(teamMemberEmail).click()
        teamMember.teamMemberDelete_icon().click()

        common.yesno_yes_btn().click()
        time.sleep(1)
        self.driver.refresh()
        time.sleep(3)

        try:
            assert self.driver.find_elements_by_xpath("//*[contains(text(),'" + teamMemberEmail + "')]")
        except:
            print("Team member has been deleted: " + teamMemberEmail)

    def createNewTeamMember_FeatureFlagTrue(self, firstName, lastName, email, teamRole, licensingType):
        # used for methods that allow for the default department
        teamMember = Teams(self.driver)
        try:
            teamMember.team_tab().click()
        except:
            print("User is already on the Team page")

        teamMember.add_new_team_member_btn().click()
        teamMember.firstName_input().send_keys(firstName)
        teamMember.lastName_input().send_keys(lastName)
        teamMember.emailAddress_input().send_keys(email)
        teamMember.role_dropDownMenu().click()
        time.sleep(1)
        actions = ActionChains(self.driver)

        if teamRole == "Staff":
            actions.move_to_element(teamMember.role_Staff_option())
            actions.click(teamMember.role_Staff_option())
            actions.perform()

        elif teamMember == "Manager":
            actions.move_to_element(teamMember.role_Manager_option())
            actions.click(teamMember.role_Manager_option())
            actions.perform()

        elif teamMember == "Administrator":
            actions.move_to_element(teamMember.role_Administrator_option())
            actions.click(teamMember.role_Administrator_option())
            actions.perform()

        actions.reset_actions()

        if licensingType == "RLM":
            teamMember.licensing_RLM_radioBox().click()
        elif licensingType == "SFS":
            teamMember.licensing_SFS_radioBox().click()
        elif licensingType == "Bundle":
            teamMember.licensing_BUNDLE_radioBox().click()
        elif licensingType == "Platform":
            teamMember.licensing_PLATFORM_radioBox().click()

        teamMember.addNewTeamMember_Submit_button().click()
