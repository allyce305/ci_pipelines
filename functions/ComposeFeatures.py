import time

from pageObjects.SFS_Compose import SFSCompose


class ComposeMethods:

    def __init__(self, driver):
        self.driver = driver

    def send_compose_message(self, name, email, subject_line, notes, file):
        compose = SFSCompose(self.driver)

        compose.name_txt().send_keys(name)
        compose.email_txt().send_keys(email)
        compose.subject_line_txt().send_keys(subject_line)
        compose.notes_txt().send_keys(notes)

        # uploads = UploadsDownloads(self.driver)
        compose.file_upload().send_keys(file)
        time.sleep(3)
        compose.send_attached_files_btn().click()
