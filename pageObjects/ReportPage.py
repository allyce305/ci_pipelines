import time

from selenium.webdriver.common.by import By


class Report:

    def __init__(self, driver):
        self.driver = driver

    # --- Locators for Files Section ---
    DownloadFiles_checkbox = (By.XPATH, "(//input[@type='radio'])[1]")
    DownloadFiles_button = (By.XPATH, "//a[normalize-space()='Download Files']")
    PreviewFIle_icon = (By.XPATH, "//*[@id='rdfl']/div/span[2]/a")

    # --- Locators for Guest Access Section ---
    InviteGuestUser_button = (By.LINK_TEXT, "+ Invite Guest User(s)")
    FirstName_text = (By.XPATH, "//input[@placeholder='First Name']")
    LastName_text = (By.XPATH, "//input[@placeholder='Last Name']")
    Email_text = (By.XPATH, "//input[@placeholder='Email']")
    AddAnother_button = (By.LINK_TEXT, "Add Another +")
    NextAccessDetails_button = (By.LINK_TEXT, "Next: Access Details >")
    Duration_Length_checkbox = (By.XPATH, "//*[@id='mmdz']/section[1]/div/div[1]/span[1]/input")
    NextFileAssignments_button = (By.LINK_TEXT, "Next: File Assignments >")
    FileAccess_InviteGuestUsers_button = (By.LINK_TEXT, "Invite Guest(s) To Report")

    def download_files_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.DownloadFiles_button)

    def download_files_chk(self):
        time.sleep(1)
        return self.driver.find_element(*Report.DownloadFiles_checkbox)

    def preview_file_icn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.PreviewFIle_icon)

    def invite_guest_user_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.InviteGuestUser_button)

    def first_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Report.FirstName_text)

    def last_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Report.LastName_text)

    def email_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Report.Email_text)

    def add_another_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.AddAnother_button)

    def next_access_details_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.NextAccessDetails_button)

    def duration_length_chk(self):
        time.sleep(1)
        return self.driver.find_element(*Report.Duration_Length_checkbox)

    def next_file_assignments_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.NextFileAssignments_button)

    def file_access_invite_guest_users_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Report.FileAccess_InviteGuestUsers_button)
