import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


class FirmTab:

    def __init__(self, driver):
        self.driver = driver

    MyFirm_tab = (By.XPATH, "//*[@id='topSelector_firm_tab']")

    # --- Locators for Automation Tab ---
    Automation_tab = (By.XPATH, "//*[@id='firmTab_5']")
    CreateNewRule_button = (By.XPATH, "(//span[contains(text(),'Create a new rule')])[1]")
    RuleName_text = (By.XPATH, "//input[@placeholder='Rule Name']")
    Action_dropdown = (By.XPATH, "//*[@id='react-select-3-input']")
    Description_text = (By.XPATH, "//input[@placeholder='Description']")
    Criteria_dropdown = (By.XPATH, "//*[@id='react-select-4-input']")
    Filter_dropdown = (By.XPATH, "//*[@id='react-select-5-input']")
    Specifications_text = (By.XPATH, "(//input[@id='react-select-2-input'])[2]")
    AddFilter_button = (By.XPATH, "//a[normalize-space()='Add Filter']")
    Timeframe_dropdown = (By.XPATH, "//input[@id='react-select-6-input']")
    After_dropdown = (By.XPATH, "//*[@id='react-select-7-input']")
    YesDeleteContinue_text = (By.XPATH, "(//input)[10]")
    CreateRule_button = (By.XPATH, "//*[@id='autoUpdates_root']/div/div[3]/div[2]/div/div[4]/div[2]")
    Edit_icon = (
        By.XPATH, "//*[name()='g' and contains(@mask,'url(#editm')]//*[name()='rect' and contains(@x,'11.8643')]")
    SaveRule_button = (By.XPATH, "(//div[@class='pBTI2zz08oe5e-FDSobDD'])[1]")
    Delete_button = (
        By.XPATH, "//*[name()='g' and contains(@mask,'url(#delet')]//*[name()='rect' and contains(@x,'11.8643')]")
    ConfirmDelete_Yes_button = (By.XPATH, "//p[normalize-space()='Yes']")
    ConfirmDelete_No_button = (By.XPATH, "//p[normalize-space()='No']")

    # --- Locators for Company Tab ---
    Company_tab = (By.XPATH, "//*[@id='firmTab_1']")
    EditLogo_button = (By.LINK_TEXT, "//*[@id='Edit Logo']")
    UploadNewImage_button = (By.XPATH, "//*[@id='uploadPhoto']")
    SaveCroppedImage_button = (By.XPATH, "//*[@id='newClient_submitBtn']")
    EnableFirmCoBranding_button = (By.XPATH, "//a[normalize-space()='Enable Firm Co-Branding']")
    YesNo_Yes_button = (By.XPATH, "//*[@id='yesNo_YesBtn']")
    DisableFirmCoBranding_button = (By.LINK_TEXT, "Disable Firm Co-Branding")
    YesNo_No_button = (By.XPATH, "//*[@id='yesNo_NoBtn']")
    SaveAgreement_button = (By.XPATH, "//td[normalize-space()='Save']")
    Active_FirmCoBranding_icon = (By.XPATH, "td[class='navSubBar firmBranding company active'] span")
    EnableEULA_button = (By.LINK_TEXT, "Enable EULA")
    Active_EULA_icon = (
        By.XPATH, "//td[@class='company navSubBar eula active']//i[contains(text(),'Active')]")
    DisableEULA_button = (By.LINK_TEXT, "Disable EULA")
    EULA_viewEula_button = (By.LINK_TEXT, "View EULA")
    EULA_Title_text = (By.XPATH, "//*[@id='title']")
    EULA_FullName_text = (By.XPATH, "//*[@id='fullName']")
    EULA_Company_text = (By.XPATH, "//*[@id='company']")
    EULA_Agree_checkbox = (By.XPATH, "//input[@id='iHaveReadAccept']")
    EULA_AcceptContract_button = (By.XPATH, "//*[@id='saveBtn']")
    EULA_Accept_yes_button = (By.XPATH, "//*[@id='yesNo_YesBtn']")
    EULA_Editor = (By.XPATH, "//iframe[@id='editor_ifr']")
    EnableRequiredClientId_button = (By.LINK_TEXT, "Enable Required Client ID")
    DisableRequiredClientId_button = (By.LINK_TEXT, "Disable Required Client ID")

    # --- Locators for Integrations Tab ---
    Integrations_tab = (By.XPATH, "//*[@id='firmTab_4']")
    ConnectAnApplication_button = (By.XPATH, "//span[normalize-space()='Connect an application']")
    ApplicationName_text = (By.XPATH, "//input[@placeholder='Application Name']")
    AcceptAndContinue_button = (By.XPATH, "//*[@id='console_root']/div/div[2]/div[2]/div[2]/div[2]")
    ViewAccessKey_button = (By.XPATH, "//*[@id='listenForm']/div[2]/div[2]/div[2]/div")
    ViewAccessKey_text = (By.XPATH, "//*[@id='listenForm']/div[2]/div[2]/div[2]/div/input")
    RegenerateAccessKey_button = (By.XPATH, "(//*[name()='rect'])[7]")
    KeyNoLongerGrantAccess_checkbox = (
        By.XPATH, "//*[@id='overlay']/div/div/div[2]/div[2]/div[1]/div/div")
    ActionCannotBeUndone_checkbox = (
        By.XPATH, "//*[@id='overlay']/div/div/div[2]/div[2]/div[2]/div/div")
    YesConfirm_button = (By.XPATH, "//*[@id='overlay']/div/div/div[2]/div[4]/div[1]")
    AddDescription_link = (By.XPATH, "//*[@id='listenForm']/div[2]/div[2]/div[1]/div[2]")
    AddDescription_text = (By.XPATH, "(//textarea)[1]")
    Back_button = (By.XPATH, "//*[@id='BG']")
    Gear_icon = (
        By.XPATH, "//*[name()='g' and contains(@mask,'url(#setti')]//*[name()='rect' and contains(@x,'0')]")
    Gear_Deactivate_button = (By.XPATH, "//span[normalize-space()='Deactivate']")
    ConnectsWillLosePermissions_checkbox = (By.XPATH, "(//*[name()='svg'])[8]")
    ReactivateAppToUseToken_checkbox = (By.XPATH, "(//*[name()='svg'])[9]")
    Delete_icon = (
        By.XPATH, "//*[name()='g' and contains(@mask,'url(#trash')]//*[name()='rect' and contains(@x,'0')]")
    ViewDocumentation_link = (By.LINK_TEXT, "View Documentation")
    AllConnectionsDestroyed_checkbox = (By.XPATH, "(//*[name()='svg'])[8]")
    AccessTokenNoLongerPermitAction_checkbox = (By.XPATH, "(//*[name()='svg'])[9]")
    YesDelete_text = (By.XPATH, "(//input)[1]")

    def disableRequiredClientId_button(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.DisableRequiredClientId_button)

    def enableRequiredClientId_button(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EnableRequiredClientId_button)

    def confirm_delete_yes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ConfirmDelete_Yes_button)

    def confirm_delete_no_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ConfirmDelete_No_button)

    def delete_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Delete_button)

    def save_rule_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.SaveRule_button)

    def edit_icn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Edit_icon)

    def select_most_recent_automation_rule(self, automation_rule_name):
        return WebDriverWait(self.driver, 20).until(
            ec.visibility_of_element_located((By.XPATH, "//*[normalize-space()='" + automation_rule_name + "'])[1]")))

    def all_connections_destroyed_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AllConnectionsDestroyed_checkbox)

    def access_token_no_longer_permit_action_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AccessTokenNoLongerPermitAction_checkbox)

    def yes_delete_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.YesDelete_text)

    def delete_icn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Delete_icon)

    def view_documentation_lnk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ViewDocumentation_link)

    def reactivate_app_to_use_token_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ReactivateAppToUseToken_checkbox)

    def connects_will_lose_permissions_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ConnectsWillLosePermissions_checkbox)

    def gear_deactivate_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Gear_Deactivate_button)

    def gear_icn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Gear_icon)

    def back_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Back_button)

    def add_description_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AddDescription_text)

    def add_description_lnk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AddDescription_link)

    def yes_confirm_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.YesConfirm_button)

    def action_cannot_be_undone_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ActionCannotBeUndone_checkbox)

    @staticmethod
    def key_no_longer_grant_access_chk():
        time.sleep(1)
        pub = ec.visibility_of_element_located(FirmTab.KeyNoLongerGrantAccess_checkbox)
        return pub

    def regenerate_access_key_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.RegenerateAccessKey_button)

    def view_access_key_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ViewAccessKey_text)

    def view_access_key_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ViewAccessKey_button)

    def accept_and_continue_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AcceptAndContinue_button)

    def application_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ApplicationName_text)

    def integrations_tab(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Integrations_tab)

    def connect_an_application_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.ConnectAnApplication_button)

    def disable_eula_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.DisableEULA_button)

    def active_eula_icn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Active_EULA_icon)

    def active_firm_co_branding_icn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Active_FirmCoBranding_icon)

    def save_agreement_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.SaveAgreement_button)

    def eula_editor(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_Editor)

    def enable_eula_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EnableEULA_button)

    def view_eula_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_viewEula_button)

    def title_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_Title_text)

    def full_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_FullName_text)

    def company_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_Company_text)

    def agree_chk(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_Agree_checkbox)

    def accept_contract_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_AcceptContract_button)

    def accept_yes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EULA_Accept_yes_button)

    def disable_firm_co_branding_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.DisableFirmCoBranding_button)

    def yesno_yes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.YesNo_Yes_button)

    def yesno_no_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.YesNo_No_button)

    def enable_firm_co_branding_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EnableFirmCoBranding_button)

    def save_cropped_image_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.SaveCroppedImage_button)

    def upload_new_image_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.UploadNewImage_button)

    def edit_logo_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.EditLogo_button)

    def company_tab(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Company_tab)

    def create_new_rule_btn(self):
        return self.driver.find_element(*FirmTab.CreateNewRule_button)

    def yes_delete_continue_txt(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.YesDeleteContinue_text)

    def after_ddl(self):
        return self.driver.find_element(*FirmTab.After_dropdown)

    def timeframe_ddl(self):
        return self.driver.find_element(*FirmTab.Timeframe_dropdown)

    def filter_ddl(self):
        return self.driver.find_element(*FirmTab.Filter_dropdown)

    def add_filter_btn(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.AddFilter_button)

    def specifications_txt(self):
        return self.driver.find_element(*FirmTab.Specifications_text)

    def criteria_ddl(self):
        return self.driver.find_element(*FirmTab.Criteria_dropdown)

    def description_txt(self):
        return self.driver.find_element(*FirmTab.Description_text)

    def action_ddl(self):
        return self.driver.find_element(*FirmTab.Action_dropdown)

    def rule_name_txt(self):
        return self.driver.find_element(*FirmTab.RuleName_text)

    @staticmethod
    def create_rule_btn():
        pub = ec.element_to_be_clickable(FirmTab.CreateRule_button)
        return pub

    def automation_tab(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.Automation_tab)

    def my_firm_tab(self):
        time.sleep(1)
        return self.driver.find_element(*FirmTab.MyFirm_tab)
