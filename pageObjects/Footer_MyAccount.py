import time
from selenium.webdriver.common.by import By


class MyAccount:

    def __init__(self, driver):
        self.driver = driver

    MyAccount = (By.LINK_TEXT, "My Account")

    # --- Locators for Personal Profile ---
    PersonalProfile_tab = (By.XPATH,
                           "//div[contains(@class, '_2-5wru750ufjpLoTqF2GVJ') and contains(., 'Personal profile')]")
    FirstName_text = (By.XPATH,
                      "//div[@class='TextField-module_label__3eVAr' and contains(., 'First "
                      "name')]//following::div[1]//input[contains(@class, 'TextField-module_input__3QFXA')]")
    LastName_text = (By.XPATH,
                     "//div[@class='TextField-module_label__3eVAr' and contains(., 'Last name')]//following::div["
                     "1]//input[contains(@class, 'TextField-module_input__3QFXA')]")
    SavePPSettings_button = (By.XPATH, "//div[@class='Button-module_content__3yxAg' and contains(., 'Save settings')]")
    Photo_button = (By.XPATH, "//div[@class='_2A7qccOJOvp_r15FeB4DEp']")
    PhotoUpload_button = (By.XPATH, "//input[@class='_2Y6QsytFkIen8EZ8qRminO']")
    PhotoSave_button = (By.XPATH,
                        "//div[@class='Button-module_content__3yxAg' and contains(., 'Save new profile')]")

    # --- Locators for Time and date ---
    TimeDate_tab = (By.XPATH, "//div[contains(@class, '_2-5wru750ufjpLoTqF2GVJ') and contains(., 'Time and date')]")
    Date_dropdown = (By.XPATH,
                     "//div[@class='Select-module_selectRoot__f9_-E' and contains(., "
                     "'Date format')]//following::div[1]//span[contains(@class, "
                     "'Select-module_selectText__4G0AR')]")
    Time_dropdown = (By.XPATH,
                     "//div[@class='Select-module_selectRoot__f9_-E' and contains(., "
                     "'Time format')]//following::div[1]//span[contains(@class, "
                     "'Select-module_selectText__4G0AR')]")

    TimeZone_dropdown = (By.XPATH,
                         "//div[@class='Select-module_selectRoot__f9_-E' and contains(., "
                         "'Time zone')]//following::div[1]//span[contains(@class, "
                         "'Select-module_selectText__4G0AR')]")
    SaveTD_button = (By.XPATH, "//div[@class='Button-module_content__3yxAg' and contains(., 'Save settings')]")

    # --- Locators for Notifications ---
    Notifications_tab = (By.XPATH,
                         "//div[contains(@class, '_2-5wru750ufjpLoTqF2GVJ') and contains(., 'Notifications')]")
    SendNotifications_button = (By.XPATH,
                                "//div[@class='Button-module_content__3yxAg' and contains(., 'Send notifications "
                                "now')]")
    Settings_dropdown = (By.XPATH,
                         "//p[@class='Select-module_selectLabel__3jI3P' and contains(., "
                         "'How often would you like')]//following::div[1]//span[contains(@class, "
                         "'Select-module_selectArrow__3UbfX')]")
    Report_checkbox = (By.XPATH,
                       "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                       "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'Report delivery notifications')]")
    Invited_checkbox = (By.XPATH,
                        "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                        "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'Invited to a new engagement')]")
    Assignments_checkbox = (By.XPATH,
                            "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                            "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'User assignments')]")
    NewFile_checkbox = (By.XPATH,
                        "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                        "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'New file upload')]")
    Comments_checkbox = (By.XPATH,
                         "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                         "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'Request comments')]")
    Status_checkbox = (By.XPATH,
                       "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                       "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'Request status change')]")
    NewRequest_checkbox = (By.XPATH,
                           "//p[@class='Text-module_textElement__1BgZG Text-module_Body2__2ZgFf "
                           "Checkbox-module_checkbox2Label__pTVzN' and contains(., 'A new request is created')]")
    SaveNotifications_button = (By.XPATH, "//div[@class='Button-module_content__3yxAg' and contains(., "
                                          "'Save settings')]")

    # --- Locators for Password ---
    Password_tab = (By.XPATH, "//p[normalize-space()='Password']")
    CurrentPassword_input = (By.XPATH, "//input[@placeholder='Enter current password']")
    NewPassword_input = (By.XPATH, "//input[@placeholder='Enter new password']")
    VerifyNewPassword_input = (By.XPATH, "//input[@placeholder='Verify new password']")
    UpdatePassword_button = (By.XPATH,
                             "//div[@class='Button-module_content__3yxAg' and contains(., 'Update password')]")

    # --- Two-factor (needs to be tested manually) ---

    # --- Footer --------
    Logout_button = (By.LINK_TEXT, "Logout")

    # --- Locators for Session settings ---
    SessionSettings_tab = (By.XPATH, "//p[normalize-space()='Session settings']")
    TimeOut_dropdown = (
        By.XPATH, "//*[@id='myAccount_root']/main/div[2]/div/div/div/div[2]/div[1]/div[2]/div")
    SaveSSSettings_button = (By.XPATH, "//p[normalize-space()='Save settings']")
    Cancel_button = (By.XPATH, "//p[normalize-space()='Cancel']")
    CurrentInactivityLogoutTime_text = (By.TAG_NAME, "h3")
    TimeOut_5Min_option = (By.XPATH, "//div[normalize-space()='5 minutes']")
    TimeOut_10Min_option = (By.XPATH, "//div[normalize-space()='10 minutes']")
    TimeOut_15Min_option = (By.XPATH, "//div[normalize-space()='15 minutes']")
    TimeOut_30Min_option = (By.XPATH, "//div[normalize-space()='30 minutes']")
    TimeOut_1Hr_option = (By.XPATH, "//div[normalize-space()='1 hour']")
    TimeOut_3Hr_option = (By.XPATH, "//div[normalize-space()='3 hours']")
    TimeOut_6Hr_option = (By.XPATH, "//div[normalize-space()='6 hours']")
    TimeOut_12Hr_option = (By.XPATH, "//div[normalize-space()='12 hours']")
    TimeOut_24Hr_option = (By.XPATH, "//div[normalize-space()='24 hours']")
    SaveConfirmation_text = (By.XPATH, "(//div[@class='MessageTile-module_top__1lNFi'])[1]")
    RLM_ExtendSession_button = (By.XPATH, "(// *[normalize-space() = 'Extend Session'])[1]")
    SFS_ExtendSession_button = (By.XPATH, "(// span[normalize-space() = 'Extend Session'])[1]")
    Footer_MyAccount_link = (By.XPATH, "(//p[normalize-space()='My Account'])[1]")

    def logout_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Logout_button)

    def footer_my_account_lnk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Footer_MyAccount_link)

    def sfs_extend_session_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SFS_ExtendSession_button)

    def rlm_extend_session_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.RLM_ExtendSession_button)

    def save_confirmation_txt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SaveConfirmation_text)

    def timeout_5min_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_5Min_option)

    def timeout_10min_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_10Min_option)

    def timeout_15min_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_15Min_option)

    def timeout_30min_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_30Min_option)

    def timeout_1hr_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_1Hr_option)

    def timeout_3hr_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_3Hr_option)

    def timeout_6hr_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_6Hr_option)

    def timeout_12hr_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_12Hr_option)

    def timeout_24hr_opt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_24Hr_option)

    def session_settings_tab(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SessionSettings_tab)

    def timeout_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeOut_dropdown)

    def save_settings_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SaveSSSettings_button)

    def current_inactivity_logout_time_txt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.CurrentInactivityLogoutTime_text)

    def cancel_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Cancel_button)

    def my_account_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.MyAccount)

    def personal_profile_tab(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.PersonalProfile_tab)

    def first_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.FirstName_text)

    def last_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.LastName_text)

    def save_pp_settings_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SavePPSettings_button)

    def photo_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Photo_button)

    def photo_upload_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.PhotoUpload_button)

    def photo_save_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.PhotoSave_button)

    def time_and_date_tab(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeDate_tab)

    def date_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Date_dropdown)

    def time_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Time_dropdown)

    def timezone_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.TimeZone_dropdown)

    def save_td_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SaveTD_button)

    def notifications_tab(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Notifications_tab)

    def send_notifications_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SendNotifications_button)

    def settings_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Settings_dropdown)

    def report_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Report_checkbox)

    def invited_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Invited_checkbox)

    def assignments_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Assignments_checkbox)

    def new_file_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.NewFile_checkbox)

    def comments_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Comments_checkbox)

    def status_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Status_checkbox)

    def new_request_chk(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.NewRequest_checkbox)

    def save_notifications_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.SaveNotifications_button)

    def password_tab(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.Password_tab)

    def current_password_input(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.CurrentPassword_input)

    def new_password_input(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.NewPassword_input)

    def verify_new_password_input(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.VerifyNewPassword_input)

    def update_password_btn(self):
        time.sleep(1)
        return self.driver.find_element(*MyAccount.UpdatePassword_button)
