import time

from selenium.webdriver.common.by import By


class ClientTab:

    def __init__(self, driver):
        self.driver = driver

    Clients_tab = (By.XPATH, "//*[@id='topSelector_clients_tab']")
    ClientsDashboard_tab = (By.XPATH, "//h3[normalize-space()='Clients Dashboard']")
    ClientsList_tab = (By.XPATH, "//h3[normalize-space()='Clients List']")
    #ClientsList_tab = (By.XPATH, "//*[@id='activity_dash_root']/main/div[2]/span[2]/h3")
    #ClientsList_tab = (By.CLASS_NAME, "(//span[@class='uik-tab__item _1U2Dl_NFL857wiUu8MGFk6 I7Y94-6cQCi5qf1F8UyJR active'])[1]")

    def clients_tab(self):
        time.sleep(1)
        return self.driver.find_element(*ClientTab.Clients_tab)

    def clients_dashboard_tab(self):
        time.sleep(1)
        return self.driver.find_element(*ClientTab.ClientsDashboard_tab)

    def clients_list_tab(self):
        time.sleep(1)
        return self.driver.find_element(*ClientTab.ClientsList_tab)
