import time

from selenium.webdriver.common.by import By


class ManageClientsDashboard:

    def __init__(self, driver):
        self.driver = driver

    # --- Locators for Universal Manage Dashboard items ---
    Close_button = (By.XPATH, "//a[@class='_1SKTskMlTUn8cz0XrqrMnm _2Nhz0z8SK_vu6Nl8ALikWG']")
    Edit_button = (By.XPATH, "//*[@id='dash-ov-modal']/div[2]/div/section[2]/div/div[1]/div[1]/div["
                             "2]/div/div/div/div/div[2]/span[2]/a[1]")
    Delete_button = (By.XPATH, "//*[@id='dash-ov-modal']/div[2]/div/section[2]/div/div[1]/div[1]/div["
                               "2]/div/div/div/div/div[2]/span[2]/a[2]")
    DeleteYes_button = (By.XPATH, "//div[@class='_2jqmsQiN_D5vmT8E2JIvl8' and contains(., 'Yes')]")
    DeleteNo_button = (By.XPATH, "//div[@class='_2jqmsQiN_D5vmT8E2JIvl8 _38HEIh_q4yQN2cObXsLx61'"
                                 "and contains(., 'No')]")
    SaveChanges_button = (By.LINK_TEXT, "Save Changes")
    DiscardChanges_button = (By.LINK_TEXT, "Discard Changes")

    # --- Locators for Engagement Lists Tab ---
    EngagementLists_tab = (By.XPATH, "//*[@id='dash-ov-modal']/div[1]/div[2]/span[1]")
    MyListsNew_button = (By.XPATH, "//span[@style='display: flex; align-items: center;' and contains(., 'My Lists')]")

    # --- Locators for Name and describe your list ---
    ListName_text = (By.XPATH, "//input[@placeholder='Name']")
    ListType_dropdown = (By.XPATH, "//span[@class='_2J4Hgm6SumFLaa8aqRz-Dj _3bxxiHU1quqpW3jxNVQWqi' and contains(., "
                                   "'LIST TYPE')]//input[starts-with(@id, 'react-select-')]")
    Description_text = (By.XPATH, "//*[@placeholder='Description']")

    # --- Locators for Search for engagements (Static list)  /  List filters (Dynamic list) ---
    Criteria_dropdown = (By.XPATH,
                         "//span[@class='_1iuICsvWe2rkaWCwQsYfk7' and contains(., 'CRITERIA')]//input[starts-with("
                         "@id, 'react-select-')]")
    Filter_dropdown = (By.XPATH,
                       "//span[@class='_1iuICsvWe2rkaWCwQsYfk7' and contains(., 'FILTER')]//input[starts-with(@id, "
                       "'react-select-')]")
    Specifications_dropdown = (By.XPATH,
                               "//span[@class='_3syoo9l2b3dFNsZ3dmTl6T' and contains(., 'SPECIFICATIONS')]//input["
                               "starts-with(@id, 'react-select-')]")
    Add_button = (By.XPATH, "//*[@class='_1SKTskMlTUn8cz0XrqrMnm _3KQeEekJi_kVCeQYP0bTtx']")
    IncludeEngagements_button = (By.XPATH, "//*[@class='_2xPPBDizTHWFzU_wcoVkdB _3-bjHDXviODA47ZuglMzrR']")

    # --- Locators for List settings ---
    WidgetSet_dropdown = (By.XPATH, "//span[@class='_2J4Hgm6SumFLaa8aqRz-Dj _23Xeu3pD7PnPPjspOU70EJ' and contains(., "
                                    "'WIDGET SET')]//input[starts-with(@id, 'react-select-')]")
    Shared_button = (By.XPATH, "//span[@class='bL8EGfXtHnqhTmocnTiG7' and contains(., 'Shared')]")
    MakeDefault_button = (By.XPATH, "//span[@class='bL8EGfXtHnqhTmocnTiG7' and contains(., 'Make Default')]")

    SaveList_button = (By.LINK_TEXT, "Save List")
    StaticList_created = (By.XPATH, "//div[@class='_2k1zCpFxV-jzYwu5nrBc33' and contains(., '[STATIC LIST]')]")
    Dynamiclist_created = (By.XPATH, "//div[@class='_2k1zCpFxV-jzYwu5nrBc33' and contains(., '[DYNAMIC LIST]')]")

    # --- Locators for Widget Sets Tab ---
    WidgetSets_tab = (
        By.XPATH, "//span[@class='uik-tab__item _1JVtvHtl4qWum-EJw71Zd1' and contains(., 'Widget Sets')]")
    MyWidgetSetsNew_button = (By.XPATH, "//div[@class='_3zBMeM-uW0Q7vLV4qGm7xj' and contains(., 'My Widget Sets')]")
    WidgetSetName_text = (By.XPATH, "//input[@placeholder='Name']")
    TimelineNotifications_widget = (By.XPATH, "//article[@id='1' and contains(., 'Timeline Notifications')]")
    ClientUploads_widget = (By.XPATH, "//article[@id='2' and contains(., 'Client Uploads')]")
    MyDownloadQueue_widget = (By.XPATH, "//article[@id='3' and contains(., 'My Download Queue')]")
    TeamDownloadQueue_widget = (By.XPATH, "//article[@id='4' and contains(., 'Team Download Queue')]")
    PastDueRequests_widget = (By.XPATH, "//article[@id='5' and contains(., 'Past Due Requests')]")
    OutstandingRequests_widget = (By.XPATH, "//article[@id='6' and contains(., 'Outstanding Requests')]")
    FulfilledRequests_widget = (By.XPATH, "//article[@id='7' and contains(., 'Fulfilled Requests')]")
    AcceptedRequests_widget = (By.XPATH, "//article[@id='8' and contains(., 'Accepted Requests')]")
    RejectedRequests_widget = (By.XPATH, "//article[@id='9' and contains(., 'Rejected Requests')]")
    UnfulfilledRequests_widget = (By.XPATH, "//article[@id='10' and contains(., 'Unfulfilled Requests')]")
    MyAssignments_widget = (By.XPATH, "//article[@id='11' and contains(., 'My Assignments')]")
    AddWidgets_dropzone = (
        By.XPATH, "//p[@class='_1LZ1IYBN5POxw7F_OuRYx2' and contains(., 'Drag a widget here to add it.')]")
    SaveWidgetSet_button = (
        By.XPATH, "//a[@class='_1SKTskMlTUn8cz0XrqrMnm _2Nhz0z8SK_vu6Nl8ALikWG' and contains(., 'Save Widget Set')]")

    # --- Locators for Activity Settings Tab ---
    ActivitySettings_tab = (
        By.XPATH, "//span[@class='uik-tab__item _1JVtvHtl4qWum-EJw71Zd1' and contains(., 'Activity Settings')]")

    def close_manage_dashboard_btn(self):
        return self.driver.find_element(*ManageClientsDashboard.Close_button)

    def engagement_lists_tab(self):
        return self.driver.find_element(*ManageClientsDashboard.EngagementLists_tab)

    def new_list_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.MyListsNew_button)

    def list_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.ListName_text)

    def list_type_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.ListType_dropdown)

    def criteria_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Criteria_dropdown)

    def filter_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Filter_dropdown)

    def specifications_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Specifications_dropdown)

    def add_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Add_button)

    def include_engagements_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.IncludeEngagements_button)

    def widget_set_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.WidgetSet_dropdown)

    def shared_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Shared_button)

    def make_default_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.MakeDefault_button)

    def edit_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Edit_button)

    def delete_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Delete_button)

    def delete_yes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.DeleteYes_button)

    def delete_no_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.DeleteNo_button)

    def save_changes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.SaveChanges_button)

    def discard_changes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.DiscardChanges_button)

    def save_list_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.SaveList_button)

    def static_list(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.StaticList_created)

    def dynamic_list(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Dynamiclist_created)

    def widget_sets_tab(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.WidgetSets_tab)

    def new_widget_sets_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.MyWidgetSetsNew_button)

    def widget_set_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.WidgetSetName_text)

    def description_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.Description_text)

    def outstanding_requests_wgt(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.OutstandingRequests_widget)

    def add_widgets_dzn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.AddWidgets_dropzone)

    def save_widget_set_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ManageClientsDashboard.SaveWidgetSet_button)
