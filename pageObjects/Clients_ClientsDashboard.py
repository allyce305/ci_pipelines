import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class ClientsDashboard:

    def __init__(self, driver):
        self.driver = driver

    ManageDashboard_button = (By.LINK_TEXT, "Manage Dashboard")
    DashboardLoaded = (By.XPATH, "//p[contains(normalize-space(.), 'LAST UPDATED AT')]")

    def manage_dashboard_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsDashboard.ManageDashboard_button)

    def dashboard_loaded(self):
        return WebDriverWait(self.driver, 10).until(ec.presence_of_element_located(ClientsDashboard.DashboardLoaded))
