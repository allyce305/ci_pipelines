import time

from selenium.webdriver.common.by import By


class CreateAccount:
    def __init__(self, driver):
        self.driver = driver

    FirstName_input = (By.XPATH, "//input[@placeholder='First Name']")
    LastName_input = (By.XPATH, "//input[@placeholder='Last Name']")
    CreatePassword_input = (By.XPATH, "//input[@placeholder='Password']")
    VerifyPassWord_input = (By.XPATH, "//input[@placeholder='Verify new password']")
    UnderstandResponsibilities_checkbox = (By.XPATH, "//*[name()='rect' and contains(@width,'18')]")
    CreateAccount_button = (By.XPATH, "(//div[@class='Button-module_content__3yxAg'])[1]")
    LiveChatSupport_button = (By.XPATH, "(//div[@class='Button-module_supportBtn__2wmsF'])[1]")

    def liveChatSupport_button(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.LiveChatSupport_button)

    def createAccount_button(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.CreateAccount_button)

    def understandResponsibilities_checkbox(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.UnderstandResponsibilities_checkbox)

    def verifyPassWord_input(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.VerifyPassWord_input)

    def createPassword_input(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.CreatePassword_input)

    def lastName_input(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.LastName_input)

    def firstName_input(self):
        time.sleep(1)
        return self.driver.find_element(*CreateAccount.FirstName_input)
