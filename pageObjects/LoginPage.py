import time

from selenium.webdriver.common.by import By


class Login:

    def __init__(self, driver):
        self.driver = driver

    UserEmail_text = (By.NAME, "email")
    UserPassword_text = (By.NAME, "password")
    SignIn_button = (By.XPATH, "//*[text()='Sign in']")
    ForgotPassword_link = (By.LINK_TEXT, "Reset it here")
    LiveChatSupport_button = (By.XPATH, "//*[text()='Live chat support']")
    Cookies_button = (By.XPATH, "//span[@class='MuiTypography-root MuiTypography-button' and contains(., 'Got It')]")
    ResetItHere_button = (By.LINK_TEXT, "Reset it here")
    SendPasswordResetEmail_button = (By.XPATH, "//button[@type='submit']")

    def send_password_reset_email_btn(self):
        self.driver.find_element(*Login.UserEmail_text).clear()
        return self.driver.find_element(*Login.SendPasswordResetEmail_button)

    def reset_it_here_btn(self):
        self.driver.find_element(*Login.UserEmail_text).clear()
        return self.driver.find_element(*Login.ResetItHere_button)

    def user_email_txt(self):
        self.driver.find_element(*Login.UserEmail_text).clear()
        return self.driver.find_element(*Login.UserEmail_text)

    def user_password_txt(self):
        self.driver.find_element(*Login.UserPassword_text).clear()
        return self.driver.find_element(*Login.UserPassword_text)

    def signin_btn(self):
        if self.driver.find_element(*Login.SignIn_button).is_enabled():
            time.sleep(1)
        return self.driver.find_element(*Login.SignIn_button)

    def expected_text(self, text):
        return self.driver.find_element_by_xpath("//*[contains(text(), '" + text + "')]")

    def forgot_password_lnk(self):
        return self.driver.find_element(*Login.ForgotPassword_link)

    def live_chat_support_btn(self):
        return self.driver.find_element(*Login.LiveChatSupport_button)

    def cookies_btn(self):
        return self.driver.find_element(*Login.Cookies_button)
