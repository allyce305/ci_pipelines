from selenium.webdriver.common.by import By


class LiveChatSupport:

    def __init__(self, driver):
        self.driver = driver

    LiveChatSupport_button = (By.XPATH, "//div[@class='Login_btn__1RrQu']")

    def live_chat_support_btn(self):
        return self.driver.find_element(*LiveChatSupport.LiveChatSupport_button)
