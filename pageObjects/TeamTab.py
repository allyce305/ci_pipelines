import time

from selenium.webdriver.common.by import By


class Teams:

    def __init__(self, driver):
        self.driver = driver

    Team_tab = (By.XPATH, "//span[normalize-space()='Team']")
    AddNewDepartment_button = (By.XPATH, "//*[@id='breadCrumbsActionButton1']")
    DepartmentName_text = (By.XPATH, "//*[@id='newGroup_name']")
    ConfirmAddNewDepartment_button = (By.XPATH, "//*[@id='newGroup_submitBtn']")
    AddNewTeamMember_button = (By.XPATH, "//*[@id='breadCrumbsActionButton']")
    TeamMemberFirstName_text = (By.XPATH, "//*[@id='newTeamMember_contactFirstName']")
    TeamMemberLastName_text = (By.XPATH, "//*[@id='newTeamMember_contactLastName']")
    TeamMemberEmail_text = (By.XPATH, "//*[@id='newTeamMember_contactEmail']")
    ConfirmAddNewTeamMember_button_text = (By.XPATH, "//*[@id='newTeamMember_submitBtn']")
    DeleteAllUsersInDepartment_checkbox = (By.XPATH, "//*[@id='deleteGroup_DeleteMembersCheck']")
    DeleteDepartment_button = (By.XPATH, "//*[@id='deleteGroup_submitBtn']")
    DeleteDepartmentConfirm_text = (By.XPATH, "//*[@id='deleteGroup_twoStageInput']")

    # -----feature flag (New Team Member modal)-------------------------------------------------------------------------------------------------
    FirstName_input = (By.XPATH, "//input[@placeholder='First Name']")
    LastName_input = (By.XPATH, "//input[@placeholder='Last Name']")
    EmailAddress_input = (By.XPATH, "//input[@placeholder='Email']")
    Role_dropDownMenu = (By.XPATH, "//div[contains(text(),'Select Team Role')]")
    # Role_Administrator_option = (By.XPATH, "//li[normalize-space()='Administrator']")
    Role_Administrator_option = (By.XPATH, "//div[@id='menu-']//li[2]")
    Role_Manager_option = (By.XPATH, "(//li[normalize-space()='Manager'])[1]")
    Role_Staff_option = (By.XPATH, "//li[normalize-space()='Staff']")
    Licensing_RLM_radioBox = (By.XPATH, "(//input[@name='radio-buttons-group'])[1]")
    Licensing_SFS_radioBox = (By.XPATH, "(//input[@name='radio-buttons-group'])[2]")
    Licensing_BUNDLE_radioBox = (By.XPATH, "(//input[@name='radio-buttons-group'])[3]")
    Licensing_PLATFORM_radioBox = (By.XPATH, "(//input[@value='0'])[1]")
    # AddNewTeamMember_Submit_button = (By.XPATH, "//p[contains(text(),'Add New Team Member')]")
    AddNewTeamMember_Submit_button = (By.TAG_NAME, "button")
    TeamMemberDelete_icon = (By.XPATH, "//span[contains(@id,'pendingInvite_')]//a[contains(text(),'delete')]")

    def teamMemberDelete_icon(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.TeamMemberDelete_icon)

    def addNewTeamMember_Submit_button(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.AddNewTeamMember_Submit_button)

    def firstName_input(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.FirstName_input)

    def lastName_input(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.LastName_input)

    def emailAddress_input(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.EmailAddress_input)

    def role_dropDownMenu(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Role_dropDownMenu)

    def role_Administrator_option(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Role_Administrator_option)

    def role_Staff_option(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Role_Staff_option)

    def role_Manager_option(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Role_Manager_option)

    def licensing_RLM_radioBox(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Licensing_RLM_radioBox)

    def licensing_SFS_radioBox(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Licensing_SFS_radioBox)

    def licensing_BUNDLE_radioBox(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Licensing_BUNDLE_radioBox)

    def licensing_PLATFORM_radioBox(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Licensing_PLATFORM_radioBox)

    def delete_all_users_in_department_chk(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.DeleteAllUsersInDepartment_checkbox)

    def delete_department_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.DeleteDepartment_button)

    def delete_department_confirm_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.DeleteDepartmentConfirm_text)

    def confirm_add_new_team_member_button_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.ConfirmAddNewTeamMember_button_text)

    def team_member_email_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.TeamMemberEmail_text)

    def team_member_last_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.TeamMemberLastName_text)

    def team_member_first_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.TeamMemberFirstName_text)

    def add_new_team_member_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.AddNewTeamMember_button)

    def confirm_add_new_department_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.ConfirmAddNewDepartment_button)

    def department_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.DepartmentName_text)

    def add_new_department_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.AddNewDepartment_button)

    def team_tab(self):
        time.sleep(1)
        return self.driver.find_element(*Teams.Team_tab)
