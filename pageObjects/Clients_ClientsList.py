import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


class ClientsList:

    def __init__(self, driver):
        self.driver = driver

    CreateClient_button = (By.CSS_SELECTOR, "#activity_dash_root > main > nav > div:nth-child(6) > a")
    CreateNewClientEngagement_button = (By.XPATH, "//*[@id='createClientFirstEngagement']")
    ClientName_text = (By.XPATH, "//*[@id='newClient_orginizationName']")
    CreateNewClient_button = (By.XPATH, "//*[@id='newClient_submitBtn']")
    EngagementName_text = (By.XPATH, "//*[@id='newEngagement_name']")
    CreateNewEngagement_button = (By.XPATH, "//*[@id='newEngagement_submitBtn']")
    ImportEngagementFromExcel_checkbox = (By.XPATH, "//*[@id='excelCheck']")
    ImportFileForEngagement = (By.XPATH, "//input[@class = 'dz-hidden-input']")
    ImportCategories_checkbox = (By.XPATH, "//*[@id='importSubCategories']")
    IdentifyCategoryImportEngagement = (By.XPATH, "//*[@id='sheet0']/tbody/tr[14]/td[1]")
    IdentifySubcategoryImportEngagement = (By.XPATH, "//*[@id='sheet0']/tbody/tr[15]/td[2]")
    IdentifyRequestNameImportEngagement = (By.XPATH, "//*[@id='sheet0']/tbody/tr[16]/td[3]")
    IdentifyRequestDescriptionImportEngagement = (By.XPATH, "//*[@id='sheet0']/tbody/tr[16]/td[4]")
    DoesntApplyToDocument_button = (By.XPATH, "//*[@id='stepDoesNotApply']")
    CreateImportEngagement_button = (By.XPATH, "//*[@id='approveExcel']/table/tbody/tr/td[1]/table/tbody/tr[1]/td")
    CloneExistingEngagement_checkbox = (By.XPATH, "//*[@id='cloneCheck']")
    InputEngagementForClone = (By.XPATH, "//input[@placeholder='Search Engagements']")
    IncludeFirmFiles_checkbox = (By.XPATH, "//*[@id='cloneFirmFiles']")
    PublishNewReport_button = (By.XPATH, "//*[@id='createClientFirstReport']")
    # ReportName_text = (By.XPATH, "//*[@id='crme']/div/span[1]/input")
    # ReportName_text = (By.XPATH, "//input[contains(@placeholder,'Report Name')]")
    ReportName_text = (By.XPATH, "//span[@class='create-form']//input[@type='text']")
    # ReportName_text = (By.XPATH, "(//input[@type='text'])[1]")

    SelectUsersReport_button = (By.LINK_TEXT, "Next: Select Users >")
    PublishReport_button = (By.LINK_TEXT, "Publish Report")
    CustomClientIdNumber_text = (By.XPATH, "//*[@id='newClient_clientIdNumber']")
    SearchForEngagementForClone_text = (By.XPATH, "//div[@id='fancybox-wrap']//span[3]")
    IncludeFirmProvidedFiles_checkbox = (By.XPATH, "//*[@id='cloneFirmFiles']")
    NewEngagementDueDate_text = (By.XPATH, "//*[@id='newEngagement_dueDate']")
    InviteClientUser_button = (By.XPATH, "//*[@id='inviteFirstClientUser']")
    ClientUserFirstName_text = (By.XPATH, "//*[@id='addUserToClient_firstName']")
    ClientUserLastName_text = (By.XPATH, "//*[@id='addUserToClient_lastName']")
    ClientUserEmailAddress_text = (By.XPATH, "//*[@id='addUserToClient_email']")
    MakeClientAdmin_checkbox = (By.XPATH, "//*[@id='inviteClientAdmin']")
    SendInvitation_button = (By.XPATH, "//*[@id='addUserToClient_submitBtn']")
    EditClient_icon = (By.LINK_TEXT, "edit")
    EditClient_SaveChanges_button = (By.XPATH, "//*[@id='newClient_submitBtn']")
    SendStatusUpdate_modal = (By.XPATH,
                              "//*[@id='dialogBox_title_content' and contains(., 'Send Status Update To Clients')]")
    StatusUpdate_button = (By.LINK_TEXT, "Status Update")
    StatusUpdate_SelectAll_checkbox = (By.XPATH, "//*[@id='statusNotifModals']/div/div/div[2]/div[1]/div[1]/label")
    DeleteClient_button = (By.XPATH, "//*[@id='clientTools']/ul/li[3]/a")
    NextSelectSpecificUsers_button = (By.XPATH, "//*[@id='alertClients_submitBtn']")
    SendEmail_button = (By.XPATH, "//span[@class='secondary']")
    ExportClientsData_modal = (By.XPATH, "//*[@id='dialogBox_title_content' and contains(., 'Export Clients Data')]")
    ExportFirm_button = (By.LINK_TEXT, "Export")
    ExportData_button = (By.XPATH, "//*[@id='exportClients_submitBtn']/span")
    Delete_button = (By.LINK_TEXT, "Delete")
    CloseAlert_button = (By.XPATH, "//*[@id='alertMessageCloseButton']")
    SecureInvitationLink_text = (By.XPATH, "//*[@id='pendingInviteUrl']")
    SecureInvitationLink_CloseAlert_button = (By.XPATH, "//*[@id='alertMessageCloseButton']")
    PendingInvitation_GetLink_button = (By.XPATH, "//tbody//div[6]")
    Close_link_icon = (By.LINK_TEXT, "Close")

    def close_link_icon(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.Close_link_icon)

    def secure_invitation_link_close_alert_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.SecureInvitationLink_CloseAlert_button)

    def secure_invitation_link_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.SecureInvitationLink_text)

    def pending_invitation_get_link_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.PendingInvitation_GetLink_button)

    def close_alert_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CloseAlert_button)

    def delete_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.Delete_button)

    def export_clients_data_mdl(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ExportClientsData_modal)

    def export_data_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ExportData_button)

    def export_firm_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ExportFirm_button)

    def send_email_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.SendEmail_button)

    def next_select_specific_users_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.NextSelectSpecificUsers_button)

    def delete_client_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.DeleteClient_button)

    def send_status_update_mdl(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.SendStatusUpdate_modal)

    def status_update_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.StatusUpdate_button)

    def status_update_select_all_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.StatusUpdate_SelectAll_checkbox)

    def edit_client_save_changes_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.EditClient_SaveChanges_button)

    def edit_client_icn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.EditClient_icon)

    def expected_text(self, text):
        time.sleep(1)
        return self.driver.find_element_by_xpath("//*[contains(text(), '" + text + "')]")

    def new_engagement_due_date_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.NewEngagementDueDate_text)

    def send_invitation_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.SendInvitation_button)

    def make_client_admin_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.MakeClientAdmin_checkbox)

    def client_user_email_address_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ClientUserEmailAddress_text)

    def client_user_last_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ClientUserLastName_text)

    def client_user_first_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ClientUserFirstName_text)

    def invite_client_user_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.InviteClientUser_button)

    def include_firm_provided_files_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IncludeFirmProvidedFiles_checkbox)

    def custom_client_id_number_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CustomClientIdNumber_text)

    def input_engagement_for_clone(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.InputEngagementForClone)

    @staticmethod
    def search_for_engagement_for_clone_text():
        pub = ec.visibility_of_element_located(ClientsList.SearchForEngagementForClone_text)
        return pub

    @staticmethod
    def publish_report_btn():
        pub = ec.visibility_of_element_located(ClientsList.PublishReport_button)
        return pub

    @staticmethod
    def select_users_report_btn():
        pub = ec.visibility_of_element_located(ClientsList.SelectUsersReport_button)
        return pub

    def report_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ReportName_text)

    def publish_new_report_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.PublishNewReport_button)

    def include_firm_files_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IncludeFirmFiles_checkbox)

    def clone_existing_engagement_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CloneExistingEngagement_checkbox)

    def create_import_engagement_btn(self):
        time.sleep(2)
        return self.driver.find_element(*ClientsList.CreateImportEngagement_button)

    def doesnt_apply_to_document_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.DoesntApplyToDocument_button)

    def identify_request_description_import_engagement(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IdentifyRequestDescriptionImportEngagement)

    def identify_request_name_import_engagement(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IdentifyRequestNameImportEngagement)

    def identify_subcategory_import_engagement(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IdentifySubcategoryImportEngagement)

    def identify_category_import_engagement(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.IdentifyCategoryImportEngagement)

    def import_categories_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ImportCategories_checkbox)

    def validate_successful_excel_file_import_on_engagement(self, file):
        try:
            WebDriverWait(self.driver, 20).until(
                ec.visibility_of_element_located((By.XPATH, "//*[contains(text(), '" + file + "')]")))
        except Exception as e:
            print("Could not import file after waiting for 20 seconds")
            print("Cannot create a engagement via excel import")
            print(e)

    def import_file_for_engagement(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ImportFileForEngagement)

    def import_engagement_from_excel_chk(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ImportEngagementFromExcel_checkbox)

    def client_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.ClientName_text)

    def create_new_client_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CreateNewClient_button)

    def create_new_engagement_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CreateNewEngagement_button)

    def engagement_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.EngagementName_text)

    def create_new_client_engagement_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CreateNewClientEngagement_button)

    def create_client_btn(self):
        time.sleep(1)
        return self.driver.find_element(*ClientsList.CreateClient_button)

    def select_engagement_lnk(self, engagement_name):
        time.sleep(1)
        return self.driver.find_element(By.LINK_TEXT, engagement_name)

    def select_report_lnk(self):
        time.sleep(1)
        print("Report link was selected")
        return self.driver.find_element(By.XPATH, "//*[@class = 'reportName']")
