from selenium.webdriver.common.by import By


class SFSCompose:

    def __init__(self, driver):
        self.driver = driver

    File_upload = (By.XPATH, "//*[@id='dz-hidden-input']")
    Name_text = (By.XPATH, "//*[@id='formName']")
    Email_text = (By.XPATH, "//*[@id='formEmail']")
    SubjectLine_text = (By.XPATH, "//*[@id='formFilesNote']")
    Notes_text = (By.XPATH, "//*[@id='formAdditional']")
    SendAttachedFiles_button = (By.XPATH, "//*[text()='Send Attached Files']")
    PrivacyPolicy_link = (By.LINK_TEXT, "Privacy Policy")
    CustomerResponsibilities_link = (By.XPATH, "(//span[@class='linkText-3C5Zq'])[1]")
    LiveChat_button = (By.XPATH, "(//h6[normalize-space()='Live Chat Support'])[1]")
    CloseCustomerResponsibilities_button = (By.XPATH, "(//span[@class='c1-1q98R'])[1]")
    CloseEditMessage_button = (By.XPATH, "//span[@class='MuiIconButton-label']//*[name()='svg']")

    def close_customer_responsibilities_btn(self):
        return self.driver.find_element(*SFSCompose.CloseCustomerResponsibilities_button)

    def file_upload(self):
        return self.driver.find_element(*SFSCompose.File_upload)

    def close_edit_message_btn(self):
        return self.driver.find_element(*SFSCompose.CloseEditMessage_button)

    def name_txt(self):
        return self.driver.find_element(*SFSCompose.Name_text)

    def email_txt(self):
        return self.driver.find_element(*SFSCompose.Email_text)

    def subject_line_txt(self):
        return self.driver.find_element(*SFSCompose.SubjectLine_text)

    def notes_txt(self):
        return self.driver.find_element(*SFSCompose.Notes_text)

    def send_attached_files_btn(self):
        return self.driver.find_element(*SFSCompose.SendAttachedFiles_button)

    def privacy_policy_lnk(self):
        return self.driver.find_element(*SFSCompose.PrivacyPolicy_link)

    def customer_responsibilities_lnk(self):
        return self.driver.find_element(*SFSCompose.CustomerResponsibilities_link)

    def live_chat_btn(self):
        return self.driver.find_element(*SFSCompose.LiveChat_button)
