import time

from selenium.webdriver.common.by import By


class PasswordReset:

    def __init__(self, driver):
        self.driver = driver

    UserNewPassword_input = (By.XPATH, "//input[@placeholder='Enter new password']")#//*[@id="root"]/div[1]/div/div[2]/div/label/div[2]/input
    VerifyNewPassword_input = (By.XPATH, "//input[@placeholder='Verify new password']")
    ChangePassword_button = (By.XPATH, "//button[@type='submit']")
    EmailAddress_input = (By.XPATH, "//input[@placeholder='email address']")

    def change_password_btn(self):
        time.sleep(1)
        return self.driver.find_element(*PasswordReset.ChangePassword_button)

    def user_new_password_input(self):
        time.sleep(1)
        return self.driver.find_element(*PasswordReset.UserNewPassword_input)

    def verify_new_password_input(self):
        time.sleep(1)
        return self.driver.find_element(*PasswordReset.VerifyNewPassword_input)

    def emmail_address_input(self):
        time.sleep(1)
        return self.driver.find_element(*PasswordReset.EmailAddress_input)