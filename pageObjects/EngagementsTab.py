import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


class Engagement:

    def __init__(self, driver):
        self.driver = driver

    CreateCategory_button = (By.CSS_SELECTOR, "#buttonBar_newCat")
    CategoryName_text = (By.XPATH, "//*[@id='newCategory_name']")
    CreateNewCategory_button = (By.XPATH, "//*[@id='newCategory_submitBtn']")
    CreateRequest_button = (By.XPATH, "//*[@id='buttonBar_newReq']")
    RequestName_text = (By.XPATH, "//*[@id='newRequest_name']")
    CreateAnotherRequest_button = (By.XPATH, "//*[@id='newRequest_submitAnotherBtn']")
    CreateNewRequest_button = (By.XPATH, "//*[@id='newRequest_submitBtn']")
    AddAssignment_button = (By.XPATH, "//*[@id='userAss']")
    FirmUserForAssignment_button = (By.XPATH, "//*[@id='userAssfirmList']")
    RequestMoreOptions_icon = (By.XPATH, "//*[@id='toolsRequestMoreOptions']")
    LockRequest_button = (By.XPATH, "//*[@id='moreRequestTools_btnLock']")
    ExportPrintRequest_button = (By.XPATH, "//*[@id='moreRequestTools_btnExport']")
    GetLinkRequest_button = (By.XPATH, "//*[@id='moreRequestTools_btnLink']")
    RequestLocked_title = (By.XPATH, "//*[@id='requestLockedTitle']/span/span")
    AddComment_text = (By.XPATH, "//*[@id='requestViewComment']")
    SendComment_button = (By.XPATH, "//*[@id='submitCommentButton']")
    ValidateSuccessfulFileUploadOnRequest = (By.XPATH, "//*[@id='clientDetails_rowFileUpload_successMessage']")
    # FirmProvidedFile_link = (By.PARTIAL_LINK_TEXT, "Firm provided")
    FirmProvidedFile_link = (By.CLASS_NAME, "firmProvidedFiles")

    FirmFiles_Close_button = (By.XPATH, "//*[@id='dialogBox']/a")
    FirmFiles_CheckAll_checkBox = (By.XPATH, "//*[@id='af_checkAll']")
    FirmFiles_DownloadSelectedFiles_button = (By.XPATH, "//*[@id='afDownload']")
    ChangeStateOutstanding_dropdown = (By.LINK_TEXT, "Mark as Outstanding")
    ChangeRequestState_dropdown = (By.XPATH, "//*[@id='changeRequestDropDown']")
    MoreAuditOptions_icon = (By.XPATH, "//*[@id='toolsMoreOptions']")
    ExportPrintEngagement_button = (By.XPATH, "//li[@id='moreTools_btnExport']")
    ExportAsExcel_button = (By.XPATH, "//*[@id='excelExport']")
    CloseExporter_button = (By.XPATH, "//a[@class='btn med-blue columnLabelBoxBtn closeBtn export']")
    RequestLocked = (By.XPATH, "//div[@class='lockBox']")
    EngagementIntroductionClose_button = (By.XPATH, "//*[@id='dialogBox']/a")

    def export_print_engagement_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.ExportPrintEngagement_button)

    def change_state_outstanding_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.ChangeStateOutstanding_dropdown)

    def close_exporter_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.CloseExporter_button)

    def export_as_excel_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.ExportAsExcel_button)

    def more_audit_options_icn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.MoreAuditOptions_icon)

    def expected_link_text(self, text):
        time.sleep(1)
        return self.driver.find_elements_by_partial_link_text(text)

    def change_request_state_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.ChangeRequestState_dropdown)

    def expected_text(self, text):
        return self.driver.find_element_by_xpath("//*[contains(text(), '" + text + "')]")

    def firm_provided_file_lnk(self):
        return self.driver.find_element(*Engagement.FirmProvidedFile_link)

    def firm_files_close_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.FirmFiles_Close_button)

    def firm_files_check_all_chk(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.FirmFiles_CheckAll_checkBox)

    def firm_files_download_selected_files_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.FirmFiles_DownloadSelectedFiles_button)

    def validate_successful_file_upload_on_request(self):
        try:
            WebDriverWait(self.driver, 10).until(
                ec.visibility_of_element_located((By.XPATH, "//*[@id='clientDetails_rowFileUpload_successMessage']")))
        except Exception as e:
            print("File Failed to Upload after waiting for 10 seconds")
            print("Test will continue to run......")
            print(e)
        time.sleep(1)

    def send_comment_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.SendComment_button)

    def add_comment_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.AddComment_text)

    def request_locked_title(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.RequestLocked_title)

    def lock_request_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.LockRequest_button)

    def export_print_request_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.ExportPrintRequest_button)

    def get_link_request_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.GetLinkRequest_button)

    def request_more_options_icn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.RequestMoreOptions_icon)

    def firm_user_for_assignment_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.FirmUserForAssignment_button)

    def add_assignment_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.AddAssignment_button)

    def create_new_request_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CreateNewRequest_button)

    def create_another_request_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CreateAnotherRequest_button)

    def request_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.RequestName_text)

    def create_request_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CreateRequest_button)

    def create_new_category_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CreateNewCategory_button)

    def category_name_txt(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CategoryName_text)

    def create_category_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Engagement.CreateCategory_button)

    def add_user_assignment(self, user_name):
        return self.driver.find_element(By.CSS_SELECTOR, "div[title='" + user_name + "'] span[class='mail notSelect']")

    def request_locked(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.RequestLocked)

    def engagement_introduction_close_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Engagement.EngagementIntroductionClose_button)
