import time

from selenium.webdriver.common.by import By


class SecureFilesSharingTab:

    def __init__(self, driver):
        self.driver = driver

    SecureFileSharing_tab = (By.XPATH, "//*[@id='topSelector_sfs_tab']")
    OpenDrawers_button = (By.XPATH, "(//*[name()='circle'])[3]")

    def secure_file_sharing_tab(self):
        time.sleep(2)
        return self.driver.find_element(*SecureFilesSharingTab.SecureFileSharing_tab)

    def open_drawers_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SecureFilesSharingTab.OpenDrawers_button)
