import time

from selenium.webdriver.common.by import By


class SFSProfile:

    def __init__(self, driver):
        self.node = None
        self.driver = driver

    OpenDrawers_button = (By.XPATH, "//*[name()='circle' and contains(@cx,'18')]")
    SendFilesDrawer_tab = (By.XPATH, "//span[contains(text(),'Send Files')]")
    DragAndDropFile_dropzone = (By.XPATH, "//*[contains(@class,'FileUpload_dropboxContent')]")
    UploadFileDragAndDrop_text = (
        By.XPATH, "//*[@id='root']/div/div[3]/div[4]/div/div[2]/div/div[1]/div[2]/div/div/div/div[1]/div/div/input")
    WhoAreYouSendingTheseTo_dropdown = (By.XPATH, "(//h6[normalize-space()='Who are you sending these to?'])[1]")
    SendFiles_RecipientName_text = (By.XPATH, "//input[@placeholder='Jane Smith']")
    SendFiles_RecipientEmail_text = (By.XPATH, "//input[@placeholder='janesmith@business.com']")
    SubjectLine_text = (By.XPATH, "//input[@placeholder='What are these files for?']")
    Notes_text = (By.TAG_NAME, "textarea")
    SecurityOptions_dropdown = (By.XPATH, "(//div[@class='SendFiles_accordionHeaderContent__n3gSV'])[3]")
    UploadComplete = (By.XPATH, "//*[@id='root']//circle[2]")
    SendLinkToViewFiles_button = (By.XPATH, "(//button[@type='button'])[7]")
    RequestFilesDrawer_tab = (By.XPATH, "//span[contains(text(),'Request Files')]")
    RequestFile_Message_text = (By.TAG_NAME, "textarea")
    RequestFile_RecipientEmail_text = (By.XPATH, "//input[@type='text']")
    SendEmail_button = (By.XPATH, "//button[.='Send Email']")
    # SendEmail_button = (By.XPATH, "//span[normalize-space()='Send Email']")
    SENT_tab = (By.XPATH, "//span[normalize-space()='Sent']")
    RECEIVED_tab = (By.XPATH, "//span[normalize-space()='Received']")
    MyLinkDrawer_tab = (By.XPATH, "//span[contains(text(),'My Link')]")
    RegenerateLink_button = (By.XPATH, "//span[normalize-space()='Regenerate link']")
    ComposeLinkURL = (By.TAG_NAME, "input")
    YesRegenerateMyLinkNow_button = (By.XPATH, "//span[normalize-space()='Yes, Regenerate My Link Now.']")
    RequestAccess_button = (By.XPATH, "//span[normalize-space()='Request Access']")
    MostRecentSentFile = (
        By.XPATH, "(//div[@class='MuiAccordionSummary-content AccordionRow_accordionSummaryContent__il_RS'])[1]")
    FileHistory_link = (By.XPATH, "//p[normalize-space()='File History']")
    SENT_PreviewFile_icon = (By.XPATH, "(//button[@type='button'])[4]")
    ViewFullDocument_button = (By.XPATH, "//span[contains(text(),'View full document')]")
    CloseFilePreview_button = (By.XPATH, "//body/div[@role='presentation']/div/div/div//*[name()='svg']")
    SentFileHistory_Close_icon = (By.XPATH, "//span[@class='FileHistoryModal_dismissIcon__3FVnW']//*[name()='svg']")
    SENT_DownloadAllFiles_button = (By.XPATH, "//span[normalize-space()='Download All Files']")
    SENT_deleteMostRecentSentFile = (By.XPATH, "(//span[@class='AccordionRow_trash__2Fyeb'])[1]")
    DeleteMessage_button = (By.XPATH, "//span[normalize-space()='Delete message']")
    YesDeleteAllFile_button = (By.XPATH, "//span[normalize-space()='Yes, delete all files']")
    RestoreDeletedMessagesFiles_link = (By.XPATH, "//span[contains(text(),'Restore Deleted Messages / Files')]")
    SendMoreRequest_button = (By.XPATH, "(//span[@class='MuiButton-label'])[1]")
    MyLinkCopy_button = (By.XPATH, "(//button[@aria-label='toggle password visibility'])[1]")
    ComposeName_text = (By.XPATH, "//input[@id='formName']")
    FirstMessageOnRestoreMessageList = (By.XPATH, "//*[@id='scrolltarget-restore']/div[2]/div/div[1]/span[3]/p")
    FirstDeleteIconOnRestoreMessageList = (By.XPATH, "//*[@id='scrolltarget-restore']/div[2]/div/div[1]/span[5]/button")
    CloseMessageRestore_button = (By.XPATH, "/html[1]/body[1]/div[2]/div[3]/div[1]/div[1]/span[1]/*[name()='svg'][1]")

    def close_message_restore_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.CloseMessageRestore_button)

    def first_delete_icon_on_restore_message_list(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.FirstDeleteIconOnRestoreMessageList)

    def first_message_on_restore_message_list(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.FirstMessageOnRestoreMessageList)

    def compose_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.ComposeName_text)

    def my_link_copy_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.MyLinkCopy_button)

    def send_more_request_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendMoreRequest_button)

    def find_element_by_normalize_space(self, text):
        self.driver.find_element_by_xpath("//p[normalize-space()='" + text + "']")

    def restore_deleted_message_by_index_btn(self, index):
        time.sleep(1)
        return self.driver.find_element_by_xpath("(//button[@type='button'])[" + index + "]")

    def restore_deleted_messages_files_lnk(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RestoreDeletedMessagesFiles_link)

    def yes_delete_all_file_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.YesDeleteAllFile_button)

    def delete_message_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.DeleteMessage_button)

    def sent_delete_most_recent_sent_file(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SENT_deleteMostRecentSentFile)

    def sent_download_all_files_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SENT_DownloadAllFiles_button)

    def close_file_preview_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.CloseFilePreview_button)

    def view_full_document_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.ViewFullDocument_button)

    def sent_preview_file_icn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SENT_PreviewFile_icon)

    def sent_file_history_close_icn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SentFileHistory_Close_icon)

    def file_history_lnk(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.FileHistory_link)

    def most_recent_sent_file(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.MostRecentSentFile)

    def request_access_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RequestAccess_button)

    def sent_tab(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SENT_tab)

    def received_tab(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RECEIVED_tab)

    def yes_regenerate_my_link_now_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.YesRegenerateMyLinkNow_button)

    def compose_link_url(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.ComposeLinkURL)

    def regenerate_link_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RegenerateLink_button)

    def my_link_drawer_tab(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.MyLinkDrawer_tab)

    def send_link_to_view_files_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendLinkToViewFiles_button)

    def send_email_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendEmail_button)
        # return WebDriverWait(self.driver, 10).until(ec.presence_of_element_located(SFSProfile.SendEmail_button))

    def request_file_message_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RequestFile_Message_text)

    def request_file_recipient_email_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RequestFile_RecipientEmail_text)

    def request_files_drawer_tab(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.RequestFilesDrawer_tab)

    def upload_file_drag_and_drop_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.UploadFileDragAndDrop_text)

    def security_options_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SecurityOptions_dropdown)

    def subject_line_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SubjectLine_text)

    def notes_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.Notes_text)

    def who_are_you_sending_these_to_ddl(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.WhoAreYouSendingTheseTo_dropdown)

    def send_files_recipient_name_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendFiles_RecipientName_text)

    def send_files_recipient_email_txt(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendFiles_RecipientEmail_text)

    def open_drawers_btn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.OpenDrawers_button)

    def send_files_drawer_tab(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.SendFilesDrawer_tab)

    def drag_and_drop_file_dzn(self):
        time.sleep(1)
        return self.driver.find_element(*SFSProfile.DragAndDropFile_dropzone)

    def upload_complete(self):
        return self.UploadComplete
