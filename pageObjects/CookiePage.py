import time

from selenium.webdriver.common.by import By


class Cookies:

    def __init__(self, driver):
        self.driver = driver

    GotIt_button = (By.XPATH, "//*[@id='btn_main']")
    CloseCurrentPopup_button = (By.XPATH, "//*[@id='dialogBox']/a")

    def got_it_btn(self):
        time.sleep(2)
        return self.driver.find_element(*Cookies.GotIt_button)

    def close_current_popup_btn(self):
        time.sleep(1)
        return self.driver.find_element(*Cookies.CloseCurrentPopup_button)
