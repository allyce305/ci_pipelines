import time

from selenium.webdriver.common.by import By


class Footer:

    def __init__(self, driver):
        self.driver = driver
        self.getUserLoggedIn = self.driver.find_element_by_xpath("//span[@id='footerLeft']")

    # userLoggedIn = (By.XPATH, "//span[@id='footerLeft']")//*[@id="myRoleIcon"]/text()
    FooterNameText = (By.XPATH, "//*[@id='footerNameText']")

    def logged_in_as_user(self):
        # return self.driver.find_element(*Footer.userLoggedIn)
        return self.getUserLoggedIn

    def footer_name_text(self):
        time.sleep(1)
        return self.driver.find_element(*Footer.FooterNameText)
